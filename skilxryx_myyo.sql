-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 09, 2021 at 01:23 AM
-- Server version: 10.3.27-MariaDB-log-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skilxryx_myyo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, '5fe43cd417801_1608793300.jpg', '2020-12-19 01:13:36', '2020-12-24 12:01:40'),
(3, '5fe43cd417801_1608793300.jpg', '2020-12-19 01:13:36', '2020-12-24 12:01:40'),
(4, '5fe43cd417801_1608793300.jpg', '2020-12-19 01:13:36', '2020-12-24 12:01:40'),
(5, '5fe43cd417801_1608793300.jpg', '2020-12-19 01:13:36', '2020-12-24 12:01:40');

-- --------------------------------------------------------

--
-- Table structure for table `community`
--

CREATE TABLE `community` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `community_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_flats` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `community`
--

INSERT INTO `community` (`id`, `community_name`, `slug`, `no_of_flats`, `country`, `state`, `city`, `postal_code`, `area`, `address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Test Content', 'test-content', '5', 'India', 'ka', 'blr', '560068', 'marathahalli', 'marathahalli', 1, '2020-12-18 13:04:29', '2020-12-18 17:57:45');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `user_id`, `title`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Test content', '5fdb38f4e6966_1608202484.jfif', '<p>fdsfdsfsd&nbsp;fdsfdsfsdfdsfdsfsdfdsfdsfsdfdsfdsfsd</p>', 1, '2020-12-17 18:54:44', '2020-12-17 19:12:50'),
(3, '2', 'Grammar Quiz', '5fe437bd09742_1608791997.jpg', '<p>In the sentence &quot;John kicked the football across industry...</p>', 1, '2020-12-17 18:54:44', '2020-12-24 11:39:57');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `user_id`, `title`, `image`, `date`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Test content', '5fdcb11d88496_1608298781.jfif', '2020-12-19', '<p>fdserwrewfdsfdsfdsfsdfsdfsdfsd</p>', 1, '2020-12-18 21:39:41', '2020-12-18 21:55:29'),
(3, '2', 'Celebrations Gallery', '5fe43abcda105_1608792764.jpg', '2020-12-19', '<p>Cum&nbsp;doctus&nbsp;civibus&nbsp;efficiantur&nbsp;in&nbsp;imperdiet&nbsp;deterruisset</p>', 1, '2020-12-18 21:39:41', '2020-12-24 11:52:44'),
(4, '2', 'Celebrations Gallery', '5fe43b2bd999d_1608792875.jpg', '2020-12-19', '<p>Cum&nbsp;doctus&nbsp;civibus&nbsp;efficiantur&nbsp;in&nbsp;imperdiet&nbsp;deterruisset</p>', 1, '2020-12-18 21:39:41', '2020-12-24 11:54:35'),
(5, '2', 'Celebrations Gallery', '5fe43b4735ef8_1608792903.jpg', '2020-12-19', '<p>Cum&nbsp;doctus&nbsp;civibus&nbsp;efficiantur&nbsp;in&nbsp;imperdiet&nbsp;deterruisset</p>', 1, '2020-12-18 21:39:41', '2020-12-24 11:55:03'),
(6, '2', 'Celebrations Gallery', '5fe43c44196a1_1608793156.jpg', '2020-12-19', '<p>Cum&nbsp;doctus&nbsp;civibus&nbsp;efficiantur&nbsp;in&nbsp;imperdiet&nbsp;deterruisset</p>', 1, '2020-12-18 21:39:41', '2020-12-24 11:59:16'),
(7, '2', 'Celebrations Gallery', '5fe43c5841597_1608793176.jpg', '2020-12-19', '<p>Cum&nbsp;doctus&nbsp;civibus&nbsp;efficiantur&nbsp;in&nbsp;imperdiet&nbsp;deterruisset</p>', 1, '2020-12-18 21:39:41', '2020-12-24 11:59:36'),
(8, '2', 'Celebrations Gallery', '5fe43c6dac54c_1608793197.jpg', '2020-12-19', '<p>Cum&nbsp;doctus&nbsp;civibus&nbsp;efficiantur&nbsp;in&nbsp;imperdiet&nbsp;deterruisset</p>', 1, '2020-12-18 21:39:41', '2020-12-24 11:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `learning_activities`
--

CREATE TABLE `learning_activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `speaker` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `learning_activities`
--

INSERT INTO `learning_activities` (`id`, `title`, `image`, `date`, `time`, `speaker`, `price`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Data Engineer', '5fe438a3a3a2d_1608792227.jpg', '2020-12-18', '07:28', 'Teacher Name', '400', 1, '2020-12-17 23:27:00', '2020-12-24 11:43:47'),
(3, 'Data Engineer', '5fe438d5aa124_1608792277.jpg', '2020-12-18', '07:28', 'Teacher Name', '400', 1, '2020-12-17 23:27:00', '2020-12-24 11:44:37'),
(4, 'Data Engineer', '5fe4390f11926_1608792335.jpg', '2020-12-18', '07:28', 'Teacher Name', '400', 1, '2020-12-17 23:27:00', '2020-12-24 11:45:35'),
(5, 'Data Engineer', '5fe4392ae5ba9_1608792362.jpg', '2020-12-18', '07:28', 'Teacher Name', '400', 1, '2020-12-17 23:27:00', '2020-12-24 11:46:02');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community_id` bigint(20) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `user_id`, `community_id`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '4', 1, '<p>dsarwrdsfdsfsdfsd dsadsadsafdsfdsfds</p>', 1, '2020-12-19 09:02:30', '2020-12-20 09:09:32'),
(2, '5', 1, '<p>Test content</p>', 1, '2020-12-20 08:51:26', '2020-12-20 08:51:26'),
(3, '7', 1, '<p>Test content</p>', 1, '2020-12-20 08:51:26', '2020-12-20 08:51:26'),
(4, '8', 1, '<p>Test content</p>', 1, '2020-12-20 08:51:26', '2020-12-20 08:51:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_12_17_101828_create_events_table', 2),
(5, '2020_12_17_120014_create_news_updates_table', 3),
(6, '2020_12_17_135614_create_learning_activities_table', 4),
(7, '2020_12_18_034638_create_community_table', 5),
(8, '2020_12_18_104527_create_gallery_table', 6),
(9, '2020_12_18_141135_create_social_initiatives_table', 7),
(10, '2020_12_18_155537_create_useful_info_table', 8),
(11, '2020_12_19_062143_create_ads_table', 9),
(12, '2020_12_19_140849_create_message_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `news_updates`
--

CREATE TABLE `news_updates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_updates`
--

INSERT INTO `news_updates` (`id`, `user_id`, `title`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Test', '5fdb4e1dcedc9_1608207901.jfif', '<p>dfsafsdgfdgfdgfdgfdgfdgdfgdfrewrew</p>', 1, '2020-12-17 20:25:01', '2020-12-17 20:29:55'),
(3, '2', 'News Scroller', '5fe437dd234cf_1608792029.jpg', 'Add your news or other current info in this text window. This window uses an HTML IFrame and Javascript for the scrolling animation.', 1, '2020-12-24 11:40:29', '2020-12-24 11:40:29'),
(4, '2', 'News Scroller', '5fe437dd234cf_1608792029.jpg', 'Add your news or other current info in this text window. This window uses an HTML IFrame and Javascript for the scrolling animation.', 1, '2020-12-24 11:40:29', '2020-12-24 11:40:29'),
(5, '2', 'News Scroller', '5fe437dd234cf_1608792029.jpg', 'Add your news or other current info in this text window. This window uses an HTML IFrame and Javascript for the scrolling animation.', 1, '2020-12-24 11:40:29', '2020-12-24 11:40:29');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_initiatives`
--

CREATE TABLE `social_initiatives` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_initiatives`
--

INSERT INTO `social_initiatives` (`id`, `user_id`, `title`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', 'Test content', '5fdcbd7df1633_1608301949.jfif', '<p>dsarewrewrwfsdfdsfsdfsd&nbsp;dsarewrewrwfsdfdsfsdfsd</p>', 1, '2020-12-18 22:32:29', '2020-12-18 22:56:26'),
(3, '2', 'AC Class Rooms', '5fe43bd8c8134_1608793048.jpg', '<p>Onec consequat sapien amet leo cur sus rhoncus. Nullam dui mi Donec at nunc enim. Proin at iaculis tellus...</p>', 1, '2020-12-18 22:32:29', '2020-12-24 11:57:28'),
(4, '2', 'Quality Staffs', '5fe43c0f7af8f_1608793103.jpg', '<p>Onec consequat sapien amet leo cur sus rhoncus. Nullam dui mi Donec at nunc enim. Proin at iaculis tellus...</p>', 1, '2020-12-18 22:32:29', '2020-12-24 11:58:23'),
(5, '2', 'AC Class Rooms', '5fe43c25e92c8_1608793125.jpg', '<p>Onec consequat sapien amet leo cur sus rhoncus. Nullam dui mi Donec at nunc enim. Proin at iaculis tellus...</p>', 1, '2020-12-18 22:32:29', '2020-12-24 11:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `thoughts`
--

CREATE TABLE `thoughts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `thoughts`
--

INSERT INTO `thoughts` (`id`, `user_id`, `title`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '2', 'U.S. Senate Seats up for Reelection in 2020', '5fe35346c4499_1608733510.jpg', 'Add your news or other current info in this text window. This window uses an HTML IFrame and Javascript for the scrolling animation.', 1, '2020-12-23 08:55:10', '2020-12-24 11:42:03'),
(3, '2', 'U.S. Senate Seats up for Reelection in 2020', '5fe35346c4499_1608733510.jpg', 'Add your news or other current info in this text window. This window uses an HTML IFrame and Javascript for the scrolling animation.', 1, '2020-12-23 08:55:10', '2020-12-24 11:42:03'),
(4, '2', 'U.S. Senate Seats up for Reelection in 2020', '5fe35346c4499_1608733510.jpg', 'Add your news or other current info in this text window. This window uses an HTML IFrame and Javascript for the scrolling animation.', 1, '2020-12-23 08:55:10', '2020-12-24 11:42:03');

-- --------------------------------------------------------

--
-- Table structure for table `useful_info`
--

CREATE TABLE `useful_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `useful_info`
--

INSERT INTO `useful_info` (`id`, `user_id`, `description`, `created_at`, `updated_at`) VALUES
(2, '1', 'fdsfrewrewrsdfgfdgfd  fdsfrewrewrsdfgfdgfd  fdsfrewrewrsdfgfdgfd', '2020-12-19 00:28:54', '2020-12-19 00:29:13'),
(3, '2', 'Cum doctus civibus efficiantur in imperdiet deterruisset', '2020-12-19 00:28:54', '2020-12-24 12:00:13'),
(4, '2', 'Cum doctus civibus efficiant', '2020-12-24 12:00:27', '2020-12-24 12:00:27'),
(5, '2', 'Cum doctus civibus efficiantur in imperdiet deterruisset', '2020-12-24 12:00:36', '2020-12-24 12:00:36'),
(6, '2', 'Cum doctus civibus efficiant', '2020-12-24 12:00:57', '2020-12-24 12:00:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `community_id` bigint(20) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `flat_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `community_id`, `first_name`, `last_name`, `email`, `mobile`, `profile_pic`, `password`, `country`, `state`, `city`, `pincode`, `user_type`, `status`, `flat_no`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Admin', NULL, 'info@techitalents.com', '9876543210', 'dsa', '$2y$10$zPgKPJF.H3B6UH/PYtxxTu8KOS.pWBiseDC6WEqG1.6PKkxcrMkO2', 'india', 'karnataka', 'bangalore', '560068', 1, 1, NULL, NULL, NULL, NULL),
(2, 1, 'Mano', 'siva', 'mano@gmail.com', '9876542332', '5fdc4fb3ba48c_1608273843.jfif', '$2y$10$UdqLEB00bpgSjX4PNwjxOepaOgMOQJCUvhdPj/FRb3t3RBdOlQp3W', 'India', 'ka', 'blr', '560068', 2, 1, NULL, NULL, '2020-12-18 14:44:03', '2020-12-19 07:22:35'),
(4, 1, 'Speaker', 'Name', 'manosiva@gmail.com', '9876542332', '5fe4453900e54_1608795449.jpg', '$2y$10$J3iMtuoj66KAz4gbr0rFm.dghPYEfLZx1L0SL3ZDBG8tLUIbX7IHG', 'India', 'ka', 'blr', '560068', 3, 1, '255', NULL, '2020-12-18 15:50:40', '2020-12-24 12:37:29'),
(5, 1, 'Speaker', 'Name', 'manosiva54@gmail.com', '9876542332', '5fe44552141f7_1608795474.jpg', '$2y$10$J3iMtuoj66KAz4gbr0rFm.dghPYEfLZx1L0SL3ZDBG8tLUIbX7IHG', 'India', 'ka', 'blr', '560068', 3, 1, '548', NULL, '2020-12-18 15:50:40', '2020-12-24 12:37:54'),
(7, 1, 'Speaker', 'Name', 'manosiva5466@gmail.com', '9876542332', '5fe4456a1f683_1608795498.png', '$2y$10$J3iMtuoj66KAz4gbr0rFm.dghPYEfLZx1L0SL3ZDBG8tLUIbX7IHG', 'India', 'ka', 'blr', '560068', 3, 1, '548', NULL, '2020-12-18 15:50:40', '2020-12-24 12:38:18'),
(8, 1, 'Speaker', 'Name', 'manosiva546@gmail.com', '9876542332', '5fe4457d15b4c_1608795517.jpg', '$2y$10$J3iMtuoj66KAz4gbr0rFm.dghPYEfLZx1L0SL3ZDBG8tLUIbX7IHG', 'India', 'ka', 'blr', '560068', 3, 1, '548', NULL, '2020-12-18 15:50:40', '2020-12-24 12:38:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `community`
--
ALTER TABLE `community`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `learning_activities`
--
ALTER TABLE `learning_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_updates`
--
ALTER TABLE `news_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `social_initiatives`
--
ALTER TABLE `social_initiatives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thoughts`
--
ALTER TABLE `thoughts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `useful_info`
--
ALTER TABLE `useful_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `community`
--
ALTER TABLE `community`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `learning_activities`
--
ALTER TABLE `learning_activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `news_updates`
--
ALTER TABLE `news_updates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `social_initiatives`
--
ALTER TABLE `social_initiatives`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `thoughts`
--
ALTER TABLE `thoughts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `useful_info`
--
ALTER TABLE `useful_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
