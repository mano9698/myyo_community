<!DOCTYPE html>
<html lang="en">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="MyYoCommunity">
    <meta name="author" content="Ansonika">
    <title>MyYoCommunity</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
    <!--<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">-->

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{ URL::asset('UI/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('UI/css/style.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('UI/css/vendors.css') }}" rel="stylesheet">
	
	 <!-- Newsfeed CSS -->
<link rel="stylesheet" href="{{ URL::asset('UI/css/news_scroll.css') }}" type="text/css">
	
	<!-- Banner CSS -->
	    <link rel="stylesheet" type="text/css" href="{{ URL::asset('UI/css/style_alt.css') }}" />
	
	<!-- ALTERNATIVE COLORS CSS -->
	<link href="#" id="colors" rel="stylesheet">
	
</head>

<body>
		
	<div id="page">
		
	<header class="header menu_fixed">
		<div id="logo">
			<a href="index.html" title="MyYoCommunity">
				<img src="{{ URL::asset('UI/img/logo-sticky.png') }}" width="211" height="57" alt="" class="logo_normal">
				<img src="{{ URL::asset('UI/img/logo.png') }}" width="211" height="57" alt="" class="logo_sticky">
			</a>
		</div>
		<ul id="top_menu">
			<!--<li><a href="admin_section/add-listing.html" class="btn_add" target="_blank">Add Listing</a></li>-->
			<li><a href="#sign-in-dialog" id="sign-in" class="login" title="Sign In">Sign In</a></li>
			<!--<li><a href="wishlist.html" class="wishlist_bt_top" title="Your wishlist">Your wishlist</a></li>-->
		</ul>
		<!-- /top_menu -->
		<a href="#menu" class="btn_mobile">
			<!--<div class="hamburger hamburger--spin" id="hamburger">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
			</div>-->
		</a>
		<nav id="menu" class="main-menu">
			<ul>
				<li><span><a href="index.html">Home</a></span></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->
	
	<main>
		<!--<section class="hero_single version_5">
			<div class="wrapper">
				<div class="container">
					<div class="row justify-content-center pt-lg-5">
						<div class="col-xl-5 col-lg-6">
							<h3>Find what you need!</h3>
							<p>Discover top rated hotels, shops and restaurants around the world</p>
							<form method="post" action="http://www.ansonika.com/sparker/grid-listings-filterscol.html">
								<div class="custom-search-input-2">
									<div class="form-group">
										<input class="form-control" type="text" placeholder="What are you looking for...">
										<i class="icon_search"></i>
									</div>
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Where">
										<i class="icon_pin_alt"></i>
									</div>
									<select class="wide">
										<option>All Categories</option>	
										<option>Shops</option>
										<option>Hotels</option>
										<option>Restaurants</option>
										<option>Bars</option>
										<option>Events</option>
										<option>Fitness</option>
									</select>
									<input type="submit" value="Search">
								</div>
							</form>
						</div>
						<div class="col-xl-5 col-lg-6 text-right d-none d-lg-block">
							<img src="img/graphic_home.png" alt="" class="img-fluid">
						</div>
					</div>

				</div>
			</div>
		</section>-->
		<section id="jms-slideshow" class="jms-slideshow">
				<div class="step" data-color="color-1" data-x="2000" data-y="1000" data-z="3000" data-rotate="-20">
					<!--<div class="jms-content">
						<h3>Just when I thought...</h3>
						<p>From fairest creatures we desire increase, that thereby beauty's rose might never die</p>
						<a class="jms-link" href="#">Read more</a>
					</div>-->
					<img src="{{ URL::asset('UI/img/graphic_home.png') }}" />
				</div>
				<div class="step" data-color="color-2" data-x="1000" data-z="2000" data-rotate="20">
					
					<!--<img src="img/graphic_home1.png" />-->
				</div>
				<div class="step" data-color="color-3" data-x="2000" data-y="1500" data-z="1000" data-rotate="20">
					
					<!--<img src="img/graphic_home2.png" />-->
				</div>
				<div class="step" data-color="color-4" data-x="3000" data-y="2000">
					
					<img src="{{ URL::asset('UI/img/graphic_home2.png') }}" />
				</div>
				<div class="step" data-color="color-5" data-x="4000" data-y="1500" data-z="1000" data-rotate="-20">
					
					<!--<img src="img/graphic_home.png" />-->
				</div>
			</section>
		<!-- /hero_single -->
		
		<div class="bg_color_1">
			<div class="container margin_80_55">
				<div class="row mb-5 pb-3">
				<div class="col-lg-3 col-sm-6">
					<div class="main_title_2 mb-0">					
                        <p class="text-left mb-1">Upcoming Events</p></div>
                        @if($Events != null)
                        <img src="/Admin/events/{{ $Events->image }}" class="img-fluid" alt="" width="400" height="266">
						<div class="wrapper">
							<h5 class="mt-2 mb-1"><a href="#">{{ $Events->title }}</a></h5>
							<p class="mb-1">{!! Str::words($Events->description, 8 ) !!}
</p>
							<a class="btn_2 rounded add_top_10" href="#">View More</a>
						</div>
							
                        @else
                        <img src="{{ URL::asset('UI/img/hotel_4.jpg') }}" class="img-fluid" alt="" width="400" height="266">
						<div class="wrapper">
							<h5 class="mt-2 mb-1"><a href="#">Grammar Quiz</a></h5>
							<p class="mb-1">In the sentence "John kicked the football across industry...
</p>
							<a class="btn_2 rounded add_top_10" href="#">View More</a>
						{{-- </div> --}}
                        @endif
				
					</div>
					<div class="col-lg-9 col-sm-6">
						<div class="row">
							<div class="col-lg-6">
						<div id="news_iframe_scroll">
<div class="news_scroll-title">
News and Updates<br>
</div>
<iframe name="NewsIFrame" src="news_scroll.html" frameborder="0" scrolling="no"></iframe>
							</div></div>
							<div class="col-lg-6">
						<div id="news_iframe_scroll">
<div class="news_scroll-title" style="background: #A030CD;">
Thought For The Day<br>
</div>
<iframe name="NewsIFrame" src="news_scroll-feed.html" frameborder="0" scrolling="no"></iframe>
							</div></div></div>
					</div>
				</div>
				<div class="main_title_3">
				<span></span>
				<h2>Learning &amp; Activities</h2>
				<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
				<a href="#">See All Activities</a>
			</div>
			<div class="row add_bottom_30">
                @if($LearningActivities != null)
                    @foreach($LearningActivities as $Learn)
                    <div class="col-lg-3 col-sm-6">
                        <a href="#" class="grid_item small">
                            <figure >
                                <img src="/Admin/learn/{{ $Learn->image }}" alt="">
                            </figure>
                            </a>
                                <h5 class="mt-2 mb-1">{{ $Learn->title }}</h5>
                                <p class="mb-1">{{ $Learn->time }}</p>
                        <p class="mb-1"><strong>{{ $Learn->speaker }}</strong></p>
                                <a class="btn_1 add_top_10" href="#">Rs.{{ $Learn->price }}</a>						
                    </div>
                    @endforeach
                @else
                <div class="col-lg-3 col-sm-6">
					<a href="#" class="grid_item small">
						<figure >
							<img src="{{ URL::asset('UI/img/shop_1.jpg') }}" alt="">
						</figure>
						</a>
							<h5 class="mt-2 mb-1">Data Engineer</h5>
							<p class="mb-1">Mon, 23 - 5:30 PM</p>
					<p class="mb-1"><strong>Teacher Name</strong></p>
							<a class="btn_1 add_top_10" href="#">Rs.299</a>						
                </div>
                @endif
			</div>
			<!-- /row -->
				<div class="main_title_3 mt-5">
				<span></span>
				<h2>Resident Speak</h2>
				<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
				<a href="#">See All Residents</a>
			</div>
			@if($Message != null)
				<div class="row add_bottom_30">
					@foreach($Message as $Messages)
						
					<?php 
						$Users = DB::table('users')
								->where('id', $Messages->user_id)
								->first();
					?>
						<div class="col-lg-3 col-sm-6 text-center">
									<img src="{{ URL::asset('UI/img/resident/resident_1.jpg') }}" alt="" class="resident_img">
									<h5 class="mt-2 mb-1">{{ $Users->first_name }} {{ $Users->last_name }}</h5>
									<p class="mb-1">{{ $Users->flat_no }}</p>
							<p class="mb-1"><strong>{!! $Messages->description !!}</strong></p>		
						</div>
					@endforeach
				</div>
			@else
			<div class="row add_bottom_30">
				
				<div class="col-lg-3 col-sm-6 text-center">
							<img src="{{ URL::asset('UI/img/resident/resident_1.jpg') }}" alt="" class="resident_img">
							<h5 class="mt-2 mb-1">Speaker Name</h5>
							<p class="mb-1">Flat No</p>
					<p class="mb-1"><strong>Message</strong></p>					
					
				</div>
				<div class="col-lg-3 col-sm-6 text-center">
							<img src="{{ URL::asset('UI/img/resident/resident_2.jpg') }}" alt="" class="resident_img">
							<h5 class="mt-2 mb-1">Speaker Name</h5>
							<p class="mb-1">Flat No</p>
					<p class="mb-1"><strong>Message</strong></p>					
					
				</div>
				<div class="col-lg-3 col-sm-6 text-center">
							<img src="{{ URL::asset('UI/img/resident/resident_3.png') }}" alt="" class="resident_img">
							<h5 class="mt-2 mb-1">Speaker Name</h5>
							<p class="mb-1">Flat No</p>
					<p class="mb-1"><strong>Message</strong></p>					
					
				</div>
				<div class="col-lg-3 col-sm-6 text-center">
							<img src="{{ URL::asset('UI/img/resident/resident_4.jpg') }}" alt="" class="resident_img">
							<h5 class="mt-2 mb-1">Speaker Name</h5>
							<p class="mb-1">Flat No</p>
					<p class="mb-1"><strong>Message</strong></p>					
					
				</div>
			</div>
			@endif
			<!-- /row -->

			<!-- /row -->
			</div>
			<!-- /container -->	
		</div>
		<!-- /bg_color_1 -->	

		<div class="container-fluid margin_30">
			<div class="container">
			<div class="main_title_3">
				<span></span>
				<h2>Celebrations Gallery</h2>
				<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
				<a href="#">See All Gallery</a>
			</div></div>
			@if($Gallery != null)
			<div id="reccomended" class="owl-carousel owl-theme">
				@foreach ($Gallery as $Galleries)
				
					<div class="item">
						<div class="strip grid">
							<figure>
								<a href="#" class="wish_bt"></a>
								<a href="#"><img src="/Admin/gallery/{{ $Galleries->image }}" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
								<small>{{ $Galleries->date }}</small>
							</figure>
							<div class="wrapper">
								<h3><a href="#">{{ $Galleries->title }}</a></h3>
								<p>{!! Str::words($Galleries->description, 8 ) !!}</p>							
							</div>
						</div>
					</div>
				
				@endforeach
			</div>
			@else
			<div id="reccomended" class="owl-carousel owl-theme">
				<div class="item">
					<div class="strip grid">
						<figure>
							<a href="#" class="wish_bt"></a>
							<a href="#"><img src="img/blog-1.jpg" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
							<small>12 Nov 2020</small>
						</figure>
						<div class="wrapper">
							<h3><a href="#">Gallery Picture</a></h3>
							<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>							
						</div>
					</div>
				</div>
				<!-- /item -->
				<div class="item">
					<div class="strip grid">
						<figure>
							<a href="#" class="wish_bt"></a>
							<a href="#"><img src="img/blog-2.jpg" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
							<small>12 Nov 2020</small>
						</figure>
						<div class="wrapper">
							<h3><a href="#">Gallery Picture</a></h3>
							<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>	</div>
					</div>
				</div>
				<!-- /item -->
				<div class="item">
					<div class="strip grid">
						<figure>
							<a href="#" class="wish_bt"></a>
							<a href="#"><img src="img/blog-3.jpg" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
							<small>12 Nov 2020</small>
						</figure>
						<div class="wrapper">
							<h3><a href="#">Gallery Picture</a></h3>
							<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>	</div>
					</div>
				</div>
				<!-- /item -->
				<div class="item">
					<div class="strip grid">
						<figure>
							<a href="#" class="wish_bt"></a>
							<a href="#"><img src="img/location_4.jpg" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
							<small>12 Nov 2020</small>
						</figure>
						<div class="wrapper">
							<h3><a href="#">Gallery Picture</a></h3>
							<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>	</div>
					</div>
				</div>
				<!-- /item -->
				<div class="item">
					<div class="strip grid">
						<figure>
							<a href="#" class="wish_bt"></a>
							<a href="#"><img src="img/location_5.jpg" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
							<small>12 Nov 2020</small>
						</figure>
						<div class="wrapper">
							<h3><a href="#">Gallery Picture</a></h3>
							<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>	</div>
					</div>
				</div>
				<!-- /item -->
				<div class="item">
					<div class="strip grid">
						<figure>
							<a href="#" class="wish_bt"></a>
							<a href="#"><img src="img/location_6.jpg" class="img-fluid" alt="" width="400" height="266"><div class="read_more"><span>Read more</span></div></a>
							<small>12 Nov 2020</small>
						</figure>
						<div class="wrapper">
							<h3><a href="#">Gallery Picture</a></h3>
							<p>Id placerat tacimates definitionem sea, prima quidam vim no. Duo nobis persecuti cu.</p>							
						</div>
					</div>
				</div>
				<!-- /item -->
			</div>
			@endif
			<!-- /carousel -->
			<!-- /container -->
		</div>
		<!-- /container -->
		<div class="bg_color_1">
			<div class="container margin_60_35">
				<div class="main_title_3">
				<span></span>
				<h2>Advertisement</h2>
				<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
			</div>
			@if($Ads != null)
			<div class="row">
			@foreach ($Ads as $Ad)
				
				<div class="col-lg-3 col-sm-6">
					<a href="#" class="grid_item">
						<figure>
							<img src="/Admin/ads/{{ $Ad->image }}" alt="" style="
							width: 100%;
						">
							<!--<div class="info">
								<small>122 Locations</small>
								<h3>Shops</h3>
							</div>-->
						</figure>
					</a>
				</div>
			
			@endforeach
			</div>
			@else
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<a href="#" class="grid_item">
						<figure>
							<img src="img/home_cat_4.jpg" alt="">
							<!--<div class="info">
								<small>122 Locations</small>
								<h3>Shops</h3>
							</div>-->
						</figure>
					</a>
				</div>
				<!-- /grid_item -->
				<div class="col-lg-3 col-sm-6">
					<a href="#" class="grid_item">
						<figure>
							<img src="img/home_cat_4.jpg" alt="">
							
						</figure>
					</a>
				</div>
				<!-- /grid_item -->
				<div class="col-lg-3 col-sm-6">
					<a href="#" class="grid_item">
						<figure>
							<img src="img/home_cat_4.jpg" alt="">
							
						</figure>
					</a>
				</div>
				<!-- /grid_item -->
				<div class="col-lg-3 col-sm-6">
					<a href="#" class="grid_item">
						<figure>
							<img src="img/home_cat_4.jpg" alt="">
							
						</figure>
					</a>
				</div>
				<!-- /grid_item -->
			</div>
			@endif
			<!-- /row -->
				<div class="main_title_3 mt-5">
				<span></span>
				<h2>Social Initiatives and Achievements</h2>
				<p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
			</div>
			@if($Social != null)
			
			<div class="row">
				@foreach ($Social as $Socials)
				<div class="col-xl-4 col-lg-6 col-md-6">
					<div class="strip grid">
							<img src="/Admin/social/{{ $Socials->image }}" class="img-fluid" alt="">						
						<div class="wrapper">
							<h3><a href="#"><i class="pe-7s-headphones"></i>{{ $Socials->title }}</a></h3>
							<p>{!! Str::words($Socials->description, 5 ) !!}</p>
						</div>
					</div>
				</div>
				@endforeach
				
			</div>
			@else
			<div class="row">
				<div class="col-xl-4 col-lg-6 col-md-6">
					<div class="strip grid">
							<img src="img/location_1.jpg" class="img-fluid" alt="">						
						<div class="wrapper">
							<h3><a href="#"><i class="pe-7s-headphones"></i> AC Class Rooms</a></h3>
							<p>Onec consequat sapien amet leo cur sus rhoncus. Nullam dui mi Donec at nunc enim. Proin at iaculis tellus...</p>
						</div>
					</div>
				</div>
				<!-- /strip grid -->
				<div class="col-xl-4 col-lg-6 col-md-6">
					<div class="strip grid">
							<img src="img/location_2.jpg" class="img-fluid" alt="">						
						<div class="wrapper">
							<h3><a href="#"><i class="pe-7s-user-female"></i> Quality Staffs</a></h3>
							<p>Onec consequat sapien amet leo cur sus rhoncus. Nullam dui mi Donec at nunc enim. Proin at iaculis tellus...</p>
						</div>
					</div>
				</div>
				<!-- /strip grid -->
				<div class="col-xl-4 col-lg-6 col-md-6">
					<div class="strip grid">
							<img src="img/location_3.jpg" class="img-fluid" alt="">						
						<div class="wrapper">
							<h3><a href="#"><i class="pe-7s-bookmarks"></i> Spacious Library</a></h3>
							<p>Onec consequat sapien amet leo cur sus rhoncus. Nullam dui mi Donec at nunc enim. Proin at iaculis tellus...</p>
						</div>
					</div>
				</div>
				<!-- /strip grid -->
			</div>
			@endif
				
			</div>
			<!-- /container -->	
			<div class="simple-marquee-container">
				<div class="marquee-sibling">
					<strong>Useful</strong> Information
				</div>
				<div class="marquee">
					<ul class="marquee-content-items">
						@if($UsefulInfo != null)
							@foreach ($UsefulInfo as $Info)
								<li>{{ $Info->description }}</li>
							@endforeach
						@else
						<li>Cum doctus civibus efficiantur in imperdiet deterruisset</li>
						<li>Cum doctus civibus efficiantur in imperdiet deterruisset</li>
						<li>Cum doctus civibus efficiant</li>
						@endif
					</ul>
				</div>
			</div>
				<div class="clearfix"></div>
		</div>
		
	</main>
	<!-- /main -->

	<footer>
		<div class="container margin_30">
			
			<!-- /row-->

			<div class="row">
				<div class="col-lg-6">

						<a href="#">myYocommunity.</a>
						All Rights Reserved
		
				</div>
				<div class="col-lg-6">
					<ul id="additional_links">
						<li><a href="#">Terms and conditions</a></li>
						<li><a href="#">Privacy</a></li>
						<li><span>© 2020 Techitalents</span></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!--/footer-->
	</div>
	<!-- page -->
	
	<!-- Sign In Popup -->
	<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">
		<div class="small-dialog-header">
			<h3>Sign In</h3>
		</div>
		<form>
			<div class="sign-in-wrapper">
				<a href="#0" class="social_bt facebook">Login with Facebook</a>
				<a href="#0" class="social_bt google">Login with Google</a>
				<div class="divider"><span>Or</span></div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" name="email" id="email">
					<i class="icon_mail_alt"></i>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" id="password" value="">
					<i class="icon_lock_alt"></i>
				</div>
				<div class="clearfix add_bottom_15">
					<div class="checkboxes float-left">
						<label class="container_check">Remember me
						  <input type="checkbox">
						  <span class="checkmark"></span>
						</label>
					</div>
					<div class="float-right mt-1"><a id="forgot" href="javascript:void(0);">Forgot Password?</a></div>
				</div>
				<div class="text-center"><input type="submit" value="Log In" class="btn_1 full-width"></div>
				<div class="text-center">
					Don’t have an account? <a href="#">Sign up</a>
				</div>
				<div id="forgot_pw">
					<div class="form-group">
						<label>Please confirm login email below</label>
						<input type="email" class="form-control" name="email_forgot" id="email_forgot">
						<i class="icon_mail_alt"></i>
					</div>
					<p>You will receive an email containing a link allowing you to reset your password to a new preferred one.</p>
					<div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
				</div>
			</div>
		</form>
		<!--form -->
	</div>
	<!-- /Sign In Popup -->
	
	<div id="toTop"></div><!-- Back to top button -->
	
	<!-- COMMON SCRIPTS -->
    <script src="{{ URL::asset('UI/js/common_scripts.js') }}"></script>
	<script src="{{ URL::asset('UI/js/functions.js') }}"></script>
	<script src="{{ URL::asset('UI/assets/validate.js') }}"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="{{ URL::asset('UI/js/marquee.js') }}"></script>


		<script>
			$(function (){

				/* Example options:
				
					let options = {
						autostart: true,
						property: 'value',
						onComplete: null,
						duration: 20000,
						padding: 10,
						marquee_class: '.marquee',
						container_class: '.simple-marquee-container',
						sibling_class: 0,
						hover: true,
						velocity: 0.1
						direction: 'right'
					}

					$('.simple-marquee-container').SimpleMarquee(options);
					
				*/

				$('.simple-marquee-container').SimpleMarquee();
				
			});

		</script>
			<script type="text/javascript" src="{{ URL::asset('UI/js/jmpress.min.js') }}"></script>
		<!-- jmslideshow plugin : extends the jmpress plugin -->
		<script type="text/javascript" src="{{ URL::asset('UI/js/jquery.jmslideshow.js') }}"></script>
		<script type="text/javascript" src="{{ URL::asset('UI/js/modernizr.custom.48780.js') }}"></script>
<script type="text/javascript">
			$(function() {
			
				$( '#jms-slideshow' ).jmslideshow();
				
			});
		</script>
</body>


</html>