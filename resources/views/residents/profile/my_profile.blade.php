@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">User Profile</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>User Profile</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>User Profile</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/residents/update_profile" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="">
                                <div class="form-group row">
                                    <div class="col-sm-12  ml-auto">
                                        <h3>1. Personal Details</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="text" value="{{ $Managers->first_name }} {{ $Managers->last_name }}" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email Address</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="email" type="text" value="{{ $Managers->email }}">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Phone No.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="mobile" type="text" value="{{ $Managers->mobile }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Profile Picture.</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="image" type="file" value="{{ $Managers->profile_pic }}" id="AddChangeImg">

                                        <div id="" class="image-previewer" data-cropzee="AddChangeImg"></div>
                                    </div>
                                </div>
                                <div class="seperator"></div>
                                
                                <div class="form-group row">
                                    <div class="col-sm-12 ml-auto">
                                        <h3>2. Address</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Country</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="country" type="text" value="{{ $Managers->country }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">City</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="city" type="text" value="{{ $Managers->city }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">State</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="state" type="text" value="{{ $Managers->state }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Postcode</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" name="pincode" type="text" value="{{ $Managers->pincode }}">
                                    </div>
                                </div>

                                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>

                            </div>
                            
                                
                                    <div class="row">
                                        
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn">Save changes</button>
                                            <button type="reset" class="btn-secondry">Cancel</button>
                                        </div>
                                    </div>
                                
                            
                        </form>
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile" action="/residents/update_password" method="POST">
                            @csrf
                            <div class="">
                                <div class="form-group row">
                                    <div class="col-sm-12 ml-auto">
                                        <h3>3. Password</h3>
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Current Password</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="password" name="newpassword" value="">
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">New Password</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="password" name="newpassword" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Re Type Password</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="password" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn">Save changes</button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div>
                            </div>
                                
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection