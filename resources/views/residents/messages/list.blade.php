@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Speakers Message</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Message</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                
                <div class="table-responsive"></div>
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Message</th>                          
                      <th width="40%">Actions</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Message)
                        @foreach($Message as $Messages)
                            <tr>
                            <th scope="row">1</th>
                            <td>{!! Str::words($Messages->description, 10 ) !!}</td>
                            
                            <td>                            
                                <a href="/message/edit_message/{{ $Messages->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            </td>
                            <td>
                                <input data-id="{{$Messages->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $Messages->status ? 'checked' : '' }}>
                            </td>
                            </tr>
                        @endforeach
                    @else
                    <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
            </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection


@section('JSScript')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  


    $(function() {
      $('.toggle-class').change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0; 
          var id = $(this).data('id'); 
           console.log(status);
          $.ajax({
              type: "POST",
              dataType: "json",
              url: '/message/change_status',
              data: {'status': status, 'id': id},
              success: function(data){
                console.log(data.success)
                alert(data.success);
              }
          });
      })
    })
  </script>
@endsection