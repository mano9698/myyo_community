@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Managers</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Managers</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="table-responsive">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Image</th>
                      <th width="20%">Managers Name</th>
                        <th width="20%">Community</th>
                        <th>Status</th>
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($Managers)
                        @foreach($Managers as $Manager)
                            <tr>
                            <th scope="row">1</th>
                            <td><div class="new-user-list"><ul><li><span class="new-users-pic">
                                                <img src="/Admin/managers/{{ $Manager->profile_pic }}" alt=""/>
                                </span></li></ul></div></td>
                            <td>{{ $Manager->first_name }} {{ $Manager->last_name }} </td>
                            <td>{{ $Manager->community_name }}</td>
                            <td>
                                <input data-id="{{$Manager->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $Manager->status ? 'checked' : '' }}>
                            </td>
                            <td>                            
                                <a href="/managers/edit_manager/{{ $Manager->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                                {{-- <a href="/managers/delete_manager/{{ $Manager->id }}" onclick="return confirm(' Are you sure. You want to delete?');" class="btn button-sm red"><i class="ti-trash"></i></a> --}}
                            </td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
                    </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection


@section('JSScript')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  


    $(function() {
      $('.toggle-class').change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0; 
          var id = $(this).data('id'); 
           console.log(status);
          $.ajax({
              type: "POST",
              dataType: "json",
              url: '/managers/change_status',
              data: {'status': status, 'id': id},
              success: function(data){
                console.log(data.success)
                alert(data.success);
              }
          });
      })
    })
  </script>
@endsection