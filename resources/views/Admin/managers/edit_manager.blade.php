@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Edit Manager</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Manager</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/managers/update_manager" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Manager First Name</label>
                                    <div>
                                        <input class="form-control" name="id" type="hidden" value="{{ $Managers->id }}">

                                        <input class="form-control" name="first_name" type="text" value="{{ $Managers->first_name }}">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Manager Last Name</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Managers->last_name }}" name="last_name">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Manager Email</label>
                                    <div>
                                        <input class="form-control" name="email" type="text" value="{{ $Managers->email }}">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Image</label>
                                    <div>
                                        <input class="form-control" type="file" value="{{ $Managers->profile_pic }}" name="image" id="AddChangeImg">

                                        <div id="" class="image-previewer" data-cropzee="AddChangeImg"></div>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Manager's Community</label>
                                    <div>
                                        <select class="form-control" name="community_id">
                                            @foreach($Community as $Communities)
                                                <option value="{{ $Communities->id }}" @if($Communities->id == $Managers->community_id) selected @endif>{{ $Communities->community_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-12">
                                    <button type="submit" class="btn"><i class="fa fa-fw fa-plus-circle"></i>Update Manager</button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection