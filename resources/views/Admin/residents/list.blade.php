@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Residents</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Residents</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="table-responsive">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Image</th>
                      <th width="20%">Residents Name</th>
                      <th width="10%">Flat No</th>
                      <th>Community</th>
                      <th>Status</th>
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Residents)
                      @foreach($Residents as $Resident)
                    <tr>
                      <th scope="row">1</th>
                      <td><div class="new-user-list"><ul><li><span class="new-users-pic">
                                <img src="/Admin/residents/{{ $Resident->profile_pic }}" alt=""/>
                          </span></li></ul></div></td>
                      <td>{{ $Resident->first_name }} {{ $Resident->last_name }}</td>
                      <td>{{ $Resident->flat_no }}</td>
                      <td>{{ $Resident->community_name }}</td>
                      <td>
                        <input data-id="{{$Resident->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $Resident->status ? 'checked' : '' }}>
                    </td>
                      <td>                            
                        <a href="/residents/edit_residents/{{ $Resident->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                         {{-- <a href="edit_event.html" class="btn button-sm red"><i class="ti-trash"></i></a> --}}
                     </td>
                    </tr>
                    @endforeach
                    @else
                    <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
                    </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection

@section('JSScript')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  


    $(function() {
      $('.toggle-class').change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0; 
          var id = $(this).data('id'); 
           console.log(status);
          $.ajax({
              type: "POST",
              dataType: "json",
              url: '/residents/change_status',
              data: {'status': status, 'id': id},
              success: function(data){
                console.log(data.success)
                alert(data.success);
              }
          });
      })
    })
  </script>
@endsection