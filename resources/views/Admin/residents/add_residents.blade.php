@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Add Residents</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Residents</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/residents/store_residents" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Resident First Name</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="first_name">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Resident Last Name</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="last_name">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Resident Email</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="email">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Image</label>
                                    <div>
                                        <input class="form-control" type="file" value="" name="image" id="AddChangeImg">

                                        <div id="" class="image-previewer" data-cropzee="AddChangeImg"></div>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Flat No</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="flat_no">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Resident's Community</label>
                                    <div>
                                        <select class="form-control" name="community_id">
                                        <option value="" selected="" disabled="">Select the Community</option>
                                        @foreach($Community as $Communities)
                                            <option value="{{ $Communities->id }}">{{ $Communities->community_name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-12">
                                    <button type="submit" class="btn"><i class="fa fa-fw fa-plus-circle"></i>Add Residents</button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection