<div class="ttr-sidebar">
    <div class="ttr-sidebar-wrapper content-scroll">
        <!-- side menu logo start -->
        <div class="ttr-sidebar-logo">
            {{-- <a href="#"><img alt="" src="{{ URL::asset('Admin/images/logo.png') }}" width="122" height="27"></a> --}}
            <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
            </div> -->
            <div class="ttr-sidebar-toggle-button">
                <i class="ti-arrow-left"></i>
            </div>
        </div>
        <!-- side menu logo end -->
        <!-- sidebar menu start -->
        <nav class="ttr-sidebar-navi">
            @if(Auth::guard('super_admin')->check())
            <ul>
                <li class="active">
                    <a href="/admin/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-agenda"></i></span>
                        <span class="ttr-label">Upcoming Events</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/events/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Events</span></a>
                        </li>
                        <li>
                            <a href="/events/add_events" class="ttr-material-button"><span class="ttr-label">Add Events</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-upload"></i></span>
                        <span class="ttr-label">News &amp; Updates</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/news/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit News</span></a>
                        </li>
                        <li>
                            <a href="/news/add_news" class="ttr-material-button"><span class="ttr-label">Add News &amp; Updates</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Learning and activities</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/learn/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Learning</span></a>
                        </li>
                        <li>
                            <a href="/learn/add_learn" class="ttr-material-button"><span class="ttr-label">Add Learning &amp; activities</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="fa fa-handshake-o"></i></span>
                        <span class="ttr-label">Community</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/community/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Community</span></a>
                        </li>
                        <li>
                            <a href="/community/add_community" class="ttr-material-button"><span class="ttr-label">Add Community</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="fa fa-user-plus"></i></span>
                        <span class="ttr-label">Managers</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/managers/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Managers</span></a>
                        </li>
                        <li>
                            <a href="/managers/add_manager" class="ttr-material-button"><span class="ttr-label">Add Managers</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="fa fa-building"></i></span>
                        <span class="ttr-label">Residents</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/residents/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Residents</span></a>
                        </li>
                        <li>
                            <a href="/residents/add_residents" class="ttr-material-button"><span class="ttr-label">Add Residents</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-gallery"></i></span>
                        <span class="ttr-label">Celebration Gallery</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/gallery/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Gallery</span></a>
                        </li>
                        <li>
                            <a href="/gallery/add_gallery" class="ttr-material-button"><span class="ttr-label">Add Gallery</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-rocket"></i></span>
                        <span class="ttr-label">Social Initiatives</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/social/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Initiatives</span></a>
                        </li>
                        <li>
                            <a href="/social/add_social" class="ttr-material-button"><span class="ttr-label">Add Social Initiatives</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-calendar"></i></span>
                        <span class="ttr-label">Useful Information</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/useful/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Information</span></a>
                        </li>
                        <li>
                            <a href="/useful/add_useful" class="ttr-material-button"><span class="ttr-label">Add Useful Information</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-archive"></i></span>
                        <span class="ttr-label">Advertisement</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/ads/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Advertisement</span></a>
                        </li>
                        <li>
                            <a href="/ads/add_ads" class="ttr-material-button"><span class="ttr-label">Add Advertisement</span></a>
                        </li>
                    </ul>
                </li>

                <li class="ttr-seperate"></li>
            </ul>
            @elseif(Auth::guard('manager')->check())
            <ul>
                <li class="active">
                    <a href="index.html" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-agenda"></i></span>
                        <span class="ttr-label">Upcoming Events</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/events/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Events</span></a>
                        </li>
                        <li>
                            <a href="/events/add_events" class="ttr-material-button"><span class="ttr-label">Add Events</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-upload"></i></span>
                        <span class="ttr-label">News &amp; Updates</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/news/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit News</span></a>
                        </li>
                        <li>
                            <a href="/news/add_news" class="ttr-material-button"><span class="ttr-label">Add News &amp; Updates</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-upload"></i></span>
                        <span class="ttr-label">Thoughts For The Day</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/thoughts/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Thoughts</span></a>
                        </li>
                        <li>
                            <a href="/thoughts/add_thoughts" class="ttr-material-button"><span class="ttr-label">Add New Thoughts</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="fa fa-building"></i></span>
                        <span class="ttr-label">Residents</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/residents/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Residents</span></a>
                        </li>
                        <li>
                            <a href="/residents/add_residents" class="ttr-material-button"><span class="ttr-label">Add Residents</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-gallery"></i></span>
                        <span class="ttr-label">Celebration Gallery</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/gallery/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Gallery</span></a>
                        </li>
                        <li>
                            <a href="/gallery/add_gallery" class="ttr-material-button"><span class="ttr-label">Add Gallery</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-rocket"></i></span>
                        <span class="ttr-label">Social Initiatives</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/social/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Initiatives</span></a>
                        </li>
                        <li>
                            <a href="/social/add_social" class="ttr-material-button"><span class="ttr-label">Add Social Initiatives</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-calendar"></i></span>
                        <span class="ttr-label">Useful Information</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/useful/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Information</span></a>
                        </li>
                        <li>
                            <a href="/useful/add_useful" class="ttr-material-button"><span class="ttr-label">Add Useful Information</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">My Profile</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/managers/profile" class="ttr-material-button"><span class="ttr-label">Edit Profile</span></a>
                        </li>
                    </ul>
                </li>

                <li class="ttr-seperate"></li>
            </ul>
            @elseif(Auth::guard('resident')->check())
            <ul>
                <li class="active">
                    <a href="index.html" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-agenda"></i></span>
                        <span class="ttr-label">Message</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/message/list" class="ttr-material-button"><span class="ttr-label">View &amp; Edit Message</span></a>
                        </li>
                        <li>
                            <a href="/message/add_message" class="ttr-material-button"><span class="ttr-label">Add Message</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">My Profile</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/residents/profile" class="ttr-material-button"><span class="ttr-label">Edit Profile</span></a>
                        </li>
                    </ul>
                </li>
                <li class="ttr-seperate"></li>
            </ul>
            @endif
            <!-- sidebar menu end -->
        </nav>
        <!-- sidebar menu end -->
    </div>
</div>
