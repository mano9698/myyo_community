@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Useful Information</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Useful Information</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="table-responsive">
                    @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Information</th>                          
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($UsefulInfo)
                        @foreach($UsefulInfo as $Useful)
                                <tr>
                                <th scope="row">1</th>
                                <td>{{ $Useful->description }}</td>
                                <td>                            
                                    <a href="/useful/edit_useful/{{ $Useful->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            <a href="/useful/delete_useful/{{ $Useful->id}}"  class="btn button-sm red" onclick="return confirm(' Are you sure. You want to delete?');"> <i class="ti-trash"></i></a>
                                </td>
                                </tr>
                        @endforeach
                    @else
                    <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
                    </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection