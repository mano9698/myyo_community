<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from educhamp.themetrades.com/demo/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:08:15 GMT -->
<head>

	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />
	
	<!-- DESCRIPTION -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<meta name="description" content="MyYoCommunity" />
	
	<!-- OG -->
	<meta property="og:title" content="MyYoCommunity" />
	<meta property="og:description" content="MyYoCommunity" />
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">
	
	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="../error-404.html" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="../images/favicon.png" />
	
	<!-- PAGE TITLE HERE ============================================= -->
	<title>MyYoCommunity </title>
	
	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--[if lt IE 9]>
	<script src="../js/html5shiv.min.js"></script>
	<script src="../js/respond.min.js"></script>
	<![endif]-->
	
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('Admin/css/assets.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('Admin/vendors/calendar/fullcalendar.css') }}">
	
	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('Admin/css/typography.css') }}">
	
	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('Admin/css/shortcodes/shortcodes.css') }}">
	
	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('Admin/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('Admin/css/dashboard.css') }}">
	<link class="skin" rel="stylesheet" type="text/css" href="{{ URL::asset('Admin/css/color/color-1.css') }}">
	
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">
	
	<!-- header start -->
	@include('Admin.common.header')
	<!-- header end -->
	<!-- Left sidebar menu start -->
	@include('Admin.common.sidebar')
	<!-- Left sidebar menu end -->

	<!--Main container start -->
	@yield('Content')
	<div class="ttr-overlay"></div>

<!-- External JavaScripts -->
<script src="{{ URL::asset('Admin/js/jquery.min.js') }}"></script>
<script src="{{URL::asset('Admin/js/custom.js')}}"></script>

<script src="{{URL::asset('Admin/js/cropzee.js')}}"></script>

<script src="{{ URL::asset('Admin/vendors/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/counter/waypoints-min.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/counter/counterup.min.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/imagesloaded/imagesloaded.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/masonry/masonry.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/masonry/filter.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/owl-carousel/owl.carousel.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/scroll/scrollbar.min.js') }}"></script>
<script src="{{ URL::asset('Admin/js/functions.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/chart/chart.min.js') }}"></script>
<script src="{{ URL::asset('Admin/js/admin.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/calendar/moment.min.js') }}"></script>
<script src="{{ URL::asset('Admin/vendors/calendar/fullcalendar.js') }}"></script>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>





<script type="text/javascript">
	
	(function($){
$(document).ready(function(){
 
     // write code here
	 $('.ckeditor').ckeditor();

});

});


    </script>

@yield('JSScript')
</body>

</html>