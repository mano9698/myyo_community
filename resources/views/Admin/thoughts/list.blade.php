@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Thoughts</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Thoughts</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif

                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Thoughts Title</th>
                      <th>Thoughts Image</th>
                      <!--<th>Date/Time</th>-->
                      <th>Description</th>
                      
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Thoughts)
                      @foreach($Thoughts as $Thought)
                        <tr>
                        <th scope="row">1</th>
                        <td>{{ $Thought->title }}</td>
                        <td><img src="/Admin/thoughts/{{ $Thought->image }}" alt="..." class="img-fluid" style="
                            width: 250px;
                        "></td>
                        <!--<td>11Nov2020/12:30PM</td>-->
                        <td>{{ Str::words($Thought->description, 10 ) }} </td>
                        
                        <td>                            
                            <a href="/thoughts/edit_thoughts/{{ $Thought->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            <a href="/thoughts/delete_thoughts/{{$Thought->id}}" onclick="return confirm(' Are you sure. You want to delete?');" class="btn button-sm red"><i class="ti-trash"></i></a>
                        </td>
                        </tr>
                    @endforeach
                    @else
                        <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection