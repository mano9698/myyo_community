@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">News</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>News</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif

                <table class="table public-user-block block">
                  <thead>
                    <tr>
                        <th width="5%">#</th>                          
                        <th width="25%">Learning &amp; Activities Title</th>
                        <th width="25%">Learning &amp; Activities Image</th>
                        <th width="10%">Date/Time</th>
                        <th width="20%">Speaker Name</th>
                        <th width="10%">Pricing</th>
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($LearningActivities)
                      @foreach($LearningActivities as $Learning)
                        <tr>
                        <th scope="row">1</th>
                        <td>{{ $Learning->title }}</td>
                        <td><img src="/Admin/learn/{{ $Learning->image }}" alt="..." class="img-fluid" style="
                            width: 250px;
                        "></td>
                        <!--<td>11Nov2020/12:30PM</td>-->
                        <td>{{ $Learning->date }}</td>

                        <td>{{ $Learning->speaker }}</td>
                        <td>{{ $Learning->price }}</td>
                    
                        <td>                            
                            <a href="/learn/edit_learn/{{ $Learning->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            <a href="/learn/delete_learn/{{$Learning->id}}" onclick="return confirm(' Are you sure. You want to delete?');" class="btn button-sm red"><i class="ti-trash"></i></a>
                        </td>
                        </tr>
                    @endforeach
                    @else
                        <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection