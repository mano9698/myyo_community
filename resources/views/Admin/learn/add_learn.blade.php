@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Learning &amp; Activities</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Learning &amp; Activities</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/learn/store_learn" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Learning &amp; Activities title</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="title">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Image</label>
                                    <div>
                                        <input class="form-control" type="file" value="" id="AddChangeImg" name="image">

                                        <div id="" class="image-previewer" data-cropzee="AddChangeImg"></div>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Date</label>
                                    <div>
                                        <input class="form-control" type="date" value="" name="date">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Time</label>
                                    <div>
                                        <input class="form-control" type="time" value="" name="time">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Speaker</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="speaker">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Price</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="price">
                                    </div>
                                </div>
                                
                                <div class="col-12">
                                    <button type="submit" class="btn"><i class="fa fa-fw fa-plus-circle"></i>Add </button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection