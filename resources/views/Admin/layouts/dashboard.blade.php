@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Dashboard</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Dashboard</li>
            </ul>
        </div>
        <!-- Card -->
        <div class="row">

            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <div class="widget-card widget-bg4">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             Total Residents
                        </h4>
                        {{-- <span class="wc-des">
                            List of Residents
                        </span> --}}
                        <span class="wc-stats counter">
                            {{ $Residents }}
                        </span>
                        <div class="progress wc-progress">
                            <div class="progress-bar" role="progressbar" style="width: {{ $Residents }};" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="wc-progress-bx">
                            <span class="wc-number ml-auto">
                                {{ $Residents }}%
                            </span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <div class="widget-card widget-bg3">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            Total Communities
                        </h4>
                        <span class="wc-stats counter">
                            {{ $Community }}
                        </span>
                        <div class="progress wc-progress">
                            <div class="progress-bar" role="progressbar" style="width: {{ $Community }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="wc-progress-bx">
                            <span class="wc-number ml-auto">
                                {{ $Community }}%
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <div class="widget-card widget-bg3">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            Total Managers
                        </h4>
                        <span class="wc-stats counter">
                            {{ $Managers }}
                        </span>
                        <div class="progress wc-progress">
                            <div class="progress-bar" role="progressbar" style="width: {{ $Managers }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="wc-progress-bx">
                            <span class="wc-number ml-auto">
                                {{ $Managers }}%
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <div class="widget-card widget-bg4">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             Total Events
                        </h4>
                        {{-- <span class="wc-des">
                            List of Residents
                        </span> --}}
                        <span class="wc-stats counter">
                            {{ $Events }}
                        </span>
                        <div class="progress wc-progress">
                            <div class="progress-bar" role="progressbar" style="width: {{ $Events }};" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="wc-progress-bx">
                            <span class="wc-number ml-auto">
                                {{ $Events }}%
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Card END -->
        <div class="row">
            <!-- Your Profile Views Chart -->

            <!-- Your Profile Views Chart END-->

            {{-- <div class="col-lg-6 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>New Residents</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="new-user-list">
                            <ul>
                                <li>
                                    <span class="new-users-pic">
                                        <img src="../assets/images/testimonials/pic1.jpg" alt=""/>
                                    </span>
                                    <span class="new-users-text">
                                        <a href="#" class="new-users-name">Anna Strong </a>
                                        <span class="new-users-info">Visual Designer,Google Inc </span>
                                    </span>
                                    <span class="new-users-btn">
                                        <a href="#" class="btn button-sm outline">Approve</a>
                                    </span>
                                </li>
                                <li>
                                    <span class="new-users-pic">
                                        <img src="../assets/images/testimonials/pic2.jpg" alt=""/>
                                    </span>
                                    <span class="new-users-text">
                                        <a href="#" class="new-users-name"> Milano Esco </a>
                                        <span class="new-users-info">Product Designer, Apple Inc </span>
                                    </span>
                                    <span class="new-users-btn">
                                        <a href="#" class="btn button-sm outline">Approve</a>
                                    </span>
                                </li>
                                <li>
                                    <span class="new-users-pic">
                                        <img src="../assets/images/testimonials/pic1.jpg" alt=""/>
                                    </span>
                                    <span class="new-users-text">
                                        <a href="#" class="new-users-name">Nick Bold  </a>
                                        <span class="new-users-info">Web Developer, Facebook Inc </span>
                                    </span>
                                    <span class="new-users-btn">
                                        <a href="#" class="btn button-sm outline">Approve</a>
                                    </span>
                                </li>
                                <li>
                                    <span class="new-users-pic">
                                        <img src="../assets/images/testimonials/pic2.jpg" alt=""/>
                                    </span>
                                    <span class="new-users-text">
                                        <a href="#" class="new-users-name">Wiltor Delton </a>
                                        <span class="new-users-info">Project Manager, Amazon Inc </span>
                                    </span>
                                    <span class="new-users-btn">
                                        <a href="#" class="btn button-sm outline">Approve</a>
                                    </span>
                                </li>
                                <li>
                                    <span class="new-users-pic">
                                        <img src="../assets/images/testimonials/pic3.jpg" alt=""/>
                                    </span>
                                    <span class="new-users-text">
                                        <a href="#" class="new-users-name">Nick Stone </a>
                                        <span class="new-users-info">Project Manager, Amazon Inc  </span>
                                    </span>
                                    <span class="new-users-btn">
                                        <a href="#" class="btn button-sm outline">Approve</a>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> --}}


        </div>
    </div>
</main>
@endsection
