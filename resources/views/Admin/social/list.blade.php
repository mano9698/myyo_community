@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Social Initiatives &amp; Achievements</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Social Initiatives &amp; Achievements</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="table-responsive">
                    @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th width="20%">Social Initiatives Title</th>
                      <th>Social Initiatives Image</th>
                      <th>Description</th>                          
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Social)
                        @foreach($Social as $Socials)
                                <tr>
                                <th scope="row">1</th>
                                <td>{{ $Socials->title }}</td>
                                <td><img src="/Admin/social/{{ $Socials->image }}" alt="..." class="img-fluid" style="width: 250px;"></td>
                                <td>{!! Str::words($Socials->description, 10 ) !!}</td>
                                
                                <td>                            
                                    <a href="/social/edit_social/{{ $Socials->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            <a href="/social/delete_social/{{ $Socials->id}}"  class="btn button-sm red" onclick="return confirm(' Are you sure. You want to delete?');"> <i class="ti-trash"></i></a>
                                </td>
                                </tr>
                        @endforeach
                    @else
                    <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
                    </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection