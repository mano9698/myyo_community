@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Events</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Events</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif

                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Event Title</th>
                      <th>Event Image</th>
                      <!--<th>Date/Time</th>-->
                      <th>Description</th>
                      
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Events)
                      @foreach($Events as $Event)
                        <tr>
                        <th scope="row">1</th>
                        <td>{{ $Event->title }}</td>
                        <td><img src="/Admin/events/{{ $Event->image }}" alt="..." class="img-fluid" style="
                            width: 250px;
                        "></td>
                        <!--<td>11Nov2020/12:30PM</td>-->
                        <td>{!! Str::words($Event->description, 10 ) !!} </td>
                        
                        <td>                            
                            <a href="/events/edit_events/{{ $Event->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            <a href="/events/delete_events/{{$Event->id}}" onclick="return confirm(' Are you sure. You want to delete?');" class="btn button-sm red"><i class="ti-trash"></i></a>
                        </td>
                        </tr>
                    @endforeach
                    @else
                        <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection