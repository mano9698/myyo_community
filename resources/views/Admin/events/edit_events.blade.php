@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Add Events</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Events</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/events/update_events" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Event title</label>
                                    <div>
                                        <input class="form-control" type="hidden" name="id" value="{{ $Events->id }}">
                                        <input class="form-control" type="text" name="title" value="{{ $Events->title }}">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Event Image</label>
                                    <div>
                                        <input class="form-control" id="AddChangeImg" name="image" type="file" value="{{ $Events->image }}">

                                        <div id="" class="image-previewer" data-cropzee="AddChangeImg"></div>
                                    </div>
                                </div>

                                <div class="form-group col-12">
                                    <label class="col-form-label">Event description</label>
                                    <div>
                                        <textarea class="form-control ckeditor" name="description">{{ $Events->description }}</textarea>
                                    </div>
                                </div>
                                
                                <div class="col-12">
                                    <button type="submit" class="btn"><i class="fa fa-fw fa-plus-circle"></i>Update Event</button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection