@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Advertisement</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Advertisement</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="table-responsive">
                    @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th width="5%">#</th>                          
                      <th width="30%">Advertisements</th>                          
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($Ads)
                        @foreach($Ads as $Ad)
                                <tr>
                                <th scope="row">1</th>
                                <td><img src="/Admin/ads/{{ $Ad->image }}" alt="..." class="img-fluid" style="
                                    width: 250px;
                                "></td>
                                <td>                            
                                    <a href="/ads/edit_ads/{{ $Ad->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            <a href="/ads/delete_ads/{{ $Ad->id}}"  class="btn button-sm red" onclick="return confirm(' Are you sure. You want to delete?');"> <i class="ti-trash"></i></a>
                                </td>
                                </tr>
                        @endforeach
                    @else
                    <tr><td>No data found.</td></tr>
                    @endif

                  </tbody>
                </table>
                    </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection