@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Community</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Community</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="table-responsive">
                    @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>                          
                      <th>Community List</th>
                      <th>No Of Flats</th>
                      <th>Postal Code</th>
                      <th>Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Community)
                        @foreach($Community as $Communities)
                            <tr>
                            <th scope="row">1</th>
                            <td>{{ $Communities->community_name }}</td>
                                <td>{{ $Communities->no_of_flats }}</td>
                                <td>{{ $Communities->postal_code }}</td>
                            <td>                            
                                <a href="/community/edit_community/{{ $Communities->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                                <a href="/community/delete_community/{{ $Communities->id }}" onclick="return confirm(' Are you sure. You want to delete?');" class="btn button-sm red"><i class="ti-trash"></i></a>
                            </td>
                            </tr>
                        @endforeach
                    @else
                    <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
                    </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection