@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Edit Community</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Community</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/community/update_community" method="POST">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Community Name</label>
                                    <div>
                                        <input class="form-control" type="hidden" value="{{ $Community->id }}" name="id">
                                        <input class="form-control" type="text" value="{{ $Community->community_name }}" name="community_name">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">No Of Flats</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Community->no_of_flats }}" name="no_of_flats">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Country</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Community->country }}" name="country">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">State</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Community->state }}" name="state">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">City</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Community->city }}" name="city">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Postal Code</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Community->postal_code }}" name="postal_code">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Area</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Community->area }}" name="area">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Address</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $Community->address }}" name="address">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn"><i class="fa fa-fw fa-plus-circle"></i>Update Community</button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection