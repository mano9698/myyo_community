@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Celebration Gallery</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Celebration Gallery</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="table-responsive">
                    @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th width="5%">#</th>                          
                      <th width="20%">Gallery Title</th>
                      <th width="25%">Gallery Image</th>
                      <th width="10%">Date</th>
                      <th width="35%">Description</th>
                      <th width="5%">Actions&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($Gallery)
                      @foreach($Gallery as $Galleries)
                        <tr>
                        <th scope="row">1</th>
                        <td>{{ $Galleries->title }}</td>
                        <td><img src="/Admin/gallery/{{ $Galleries->image }}" alt="..." class="img-fluid" style="
                            width: 250px;
                        "></td>
                        <td>{{ $Galleries->date }}</td>
                        <td>{!! Str::words($Galleries->description, 10 ) !!} </td>
                        <td>                            
                            <a href="/gallery/edit_gallery/{{ $Galleries->id }}" class="btn button-sm blue"><i class="ti-pencil"></i></a>
                            <a href="/gallery/delete_gallery/{{ $Galleries->id}}"  class="btn button-sm red" onclick="return confirm(' Are you sure. You want to delete?');"> <i class="ti-trash"></i></a>
                        </td>
                        </tr>
                    @endforeach
                    @else
                        <tr><td>No data found.</td></tr>
                    @endif
                  </tbody>
                </table>
                    </div>
            </div>
            <!-- Your Events List END-->
        </div>
    </div>
</main>
@endsection