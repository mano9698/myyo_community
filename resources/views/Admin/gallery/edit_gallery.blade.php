@extends('Admin.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Celebration Gallery</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Celebration Gallery</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/gallery/update_gallery" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Celebration Gallery title</label>
                                    <div>
                                        <input class="form-control" name="id" type="hidden" value="{{ $Gallery->id }}">

                                        <input class="form-control" name="title" type="text" value="{{ $Gallery->title }}">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Image</label>
                                    <div>
                                        <input class="form-control" name="image" type="file" id="AddChangeImg" value="{{ $Gallery->image }}">

                                        <div id="" class="image-previewer" data-cropzee="AddChangeImg"></div>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Date</label>
                                    <div>
                                        <input class="form-control" name="date" type="date" value="{{ $Gallery->date }}">
                                    </div>
                                </div>

                                <div class="form-group col-12">
                                    <label class="col-form-label">Description</label>
                                    <div>
                                        <textarea name="description" class="form-control ckeditor">{{ $Gallery->description }} </textarea>
                                    </div>
                                </div>
                                
                                <div class="col-12">
                                    <button type="submit" class="btn"><i class="fa fa-fw fa-plus-circle"></i>Update </button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection