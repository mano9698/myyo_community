<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'UI\AuthendicationController@login');

Route::post('/login', 'UI\AuthendicationController@admin_login');

Route::get('/manager_logout', 'UI\AdminController@manager_logout');

Route::get('/resident_logout', 'UI\AdminController@resident_logout');

Route::get('/admin_logout', 'UI\AdminController@admin_logout');

Route::group(['prefix' => '/admin'], function () {

    Route::get('/dashboard', 'UI\AdminController@dashboard');
    
});


Route::group(['prefix' => '/managers'], function () {

    Route::get('/dashboard', 'UI\AdminController@dashboard');

    Route::get('/profile', 'UI\ManagerController@my_profile');

    Route::post('/update_profile', 'UI\ManagerController@update_profile');

    Route::post('/update_password', 'UI\ManagerController@update_password');
    
});


Route::group(['prefix' => '/residents'], function () {

    Route::get('/dashboard', 'UI\AdminController@dashboard');

    Route::get('/profile', 'UI\ResidentController@my_profile');

    Route::post('/update_profile', 'UI\ResidentController@update_profile');

    Route::post('/update_password', 'UI\ResidentController@update_password');
    
});



Route::group(['prefix' => '/events'], function () {

    Route::get('/list', 'UI\EventsController@list');

    Route::get('/add_events', 'UI\EventsController@add_events');

    Route::get('/edit_events/{id}', 'UI\EventsController@edit_events');

    Route::post('/store_events', 'UI\EventsController@store_events');

    Route::post('/update_events', 'UI\EventsController@update_events');

    Route::get('/delete_events/{id}', 'UI\EventsController@delete_events');
    
});

Route::group(['prefix' => '/news'], function () {

    Route::get('/list', 'UI\NewsController@list');

    Route::get('/add_news', 'UI\NewsController@add_news');

    Route::get('/edit_news/{id}', 'UI\NewsController@edit_news');

    Route::post('/store_news', 'UI\NewsController@store_news');

    Route::post('/update_news', 'UI\NewsController@update_news');

    Route::get('/delete_news/{id}', 'UI\NewsController@delete_news');
    
});

Route::group(['prefix' => '/thoughts'], function () {

    Route::get('/list', 'UI\ThoughtsController@list');

    Route::get('/add_thoughts', 'UI\ThoughtsController@add_thoughts');

    Route::get('/edit_thoughts/{id}', 'UI\ThoughtsController@edit_thoughts');

    Route::post('/store_thoughts', 'UI\ThoughtsController@store_thoughts');

    Route::post('/update_thoughts', 'UI\ThoughtsController@update_thoughts');

    Route::get('/delete_thoughts/{id}', 'UI\ThoughtsController@delete_thoughts');
    
});

Route::group(['prefix' => '/learn'], function () {

    Route::get('/list', 'UI\LearningActivitiesController@list');

    Route::get('/add_learn', 'UI\LearningActivitiesController@add_learn');

    Route::get('/edit_learn/{id}', 'UI\LearningActivitiesController@edit_learn');

    Route::post('/store_learn', 'UI\LearningActivitiesController@store_learn');

    Route::post('/update_learn', 'UI\LearningActivitiesController@update_learn');

    Route::get('/delete_learn/{id}', 'UI\LearningActivitiesController@delete_learn');
    
});

Route::group(['prefix' => '/community'], function () {

    Route::get('/list', 'UI\CommunityController@list');

    Route::get('/add_community', 'UI\CommunityController@add_community');

    Route::get('/edit_community/{id}', 'UI\CommunityController@edit_community');

    Route::post('/store_community', 'UI\CommunityController@store_community');

    Route::post('/update_community', 'UI\CommunityController@update_community');

    Route::get('/delete_community/{id}', 'UI\CommunityController@delete_community');
    
});

Route::group(['prefix' => '/managers'], function () {

    Route::get('/list', 'UI\ManagersController@list');

    Route::get('/add_manager', 'UI\ManagersController@add_manager');

    Route::get('/edit_manager/{id}', 'UI\ManagersController@edit_manager');

    Route::post('/store_manager', 'UI\ManagersController@store_manager');

    Route::post('/update_manager', 'UI\ManagersController@update_manager');

    Route::post('/change_status', 'UI\ManagersController@change_status');
    
});

Route::group(['prefix' => '/residents'], function () {

    Route::get('/list', 'UI\ResidentsController@list');

    Route::get('/add_residents', 'UI\ResidentsController@add_residents');

    Route::get('/edit_residents/{id}', 'UI\ResidentsController@edit_residents');

    Route::post('/store_residents', 'UI\ResidentsController@store_residents');

    Route::post('/update_residents', 'UI\ResidentsController@update_residents');

    Route::post('/change_status', 'UI\ResidentsController@change_status');
    
});


Route::group(['prefix' => '/gallery'], function () {

    Route::get('/list', 'UI\GalleryController@list');

    Route::get('/add_gallery', 'UI\GalleryController@add_gallery');

    Route::get('/edit_gallery/{id}', 'UI\GalleryController@edit_gallery');

    Route::post('/store_gallery', 'UI\GalleryController@store_gallery');

    Route::post('/update_gallery', 'UI\GalleryController@update_gallery');

    Route::get('/delete_gallery/{id}', 'UI\GalleryController@delete_gallery');
    
});

Route::group(['prefix' => '/social'], function () {

    Route::get('/list', 'UI\SocialController@list');

    Route::get('/add_social', 'UI\SocialController@add_social');

    Route::get('/edit_social/{id}', 'UI\SocialController@edit_social');

    Route::post('/store_social', 'UI\SocialController@store_social');

    Route::post('/update_social', 'UI\SocialController@update_social');

    Route::get('/delete_social/{id}', 'UI\SocialController@delete_social');
    
});


Route::group(['prefix' => '/useful'], function () {

    Route::get('/list', 'UI\SocialController@useful_list');

    Route::get('/add_useful', 'UI\SocialController@add_useful');

    Route::get('/edit_useful/{id}', 'UI\SocialController@edit_useful');

    Route::post('/store_useful', 'UI\SocialController@store_useful');

    Route::post('/update_useful', 'UI\SocialController@update_useful');

    Route::get('/delete_useful/{id}', 'UI\SocialController@delete_useful');
    
});

Route::group(['prefix' => '/ads'], function () {

    Route::get('/list', 'UI\AdminController@ads_list');

    Route::get('/add_ads', 'UI\AdminController@add_ads');

    Route::get('/edit_ads/{id}', 'UI\AdminController@edit_ads');

    Route::post('/store_ads', 'UI\AdminController@store_ads');

    Route::post('/update_ads', 'UI\AdminController@update_ads');

    Route::get('/delete_ads/{id}', 'UI\AdminController@delete_ads');
    
});


Route::group(['prefix' => '/message'], function () {

    Route::get('/list', 'UI\ResidentController@message_list');

    Route::get('/add_message', 'UI\ResidentController@add_message');

    Route::get('/edit_message/{id}', 'UI\ResidentController@edit_message');

    Route::post('/store_message', 'UI\ResidentController@store_message');

    Route::post('/update_message', 'UI\ResidentController@update_message');

    Route::post('/change_status', 'UI\ResidentController@change_status');
    
});




// Website Frontend
Route::get('/{slug}', 'UI\HomeController@community_website');