<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsUpdates extends Model
{
    use HasFactory;

    protected $table = 'news_updates';

    protected $fillable = ['user_id', 'title', 'image', 'description'];
}
