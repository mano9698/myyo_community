<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = ['community_id','first_name', 'last_name', 'email', 'mobile', 'profile_pic', 'password', 'user_type', 'country', 'state', 'city', 'area', 'pincode', 'status', 'flat_no', 'user_type'];
}
