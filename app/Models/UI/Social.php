<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    use HasFactory;

    protected $table = 'social_initiatives';

    protected $fillable = ['user_id', 'title', 'image','description', 'status'];
}
