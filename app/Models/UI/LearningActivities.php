<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LearningActivities extends Model
{
    use HasFactory;

    protected $table = 'learning_activities';

    protected $fillable = ['title', 'image', 'date', 'time', 'speaker', 'price', 'status'];
}
