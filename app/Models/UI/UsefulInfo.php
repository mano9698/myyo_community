<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsefulInfo extends Model
{
    use HasFactory;

    protected $table = 'useful_info';

    protected $fillable = ['user_id','description'];
}
