<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    use HasFactory;

    protected $table = 'community';

    protected $fillable = ['community_name','slug', 'no_of_flats', 'country', 'state', 'city', 'postal_code', 'area', 'address', 'status'];
}
