<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thoughts extends Model
{
    use HasFactory;

    protected $table = 'thoughts';

    protected $fillable = ['user_id', 'title', 'image', 'description'];
}
