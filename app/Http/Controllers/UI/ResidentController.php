<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\Message;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class ResidentController extends Controller
{
    public function message_list(){
        
        $title = "Messages List";
        $UserId = Session::get('ResidentId');
        $Message = Message::where('user_id', $UserId)->get();

        // $Groups = Groups::count();
        return view('residents.messages.list', compact('title', 'Message'));
    }

    public function add_message(){
        
        $title = "Add message";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('residents.messages.add_message', compact('title'));
    }

    public function edit_message($id){
        
        $title = "Edit message";
        // $UserId = Session::get('TeacherId');  

        $Message = Message::where('id', $id)->first();
        return view('residents.messages.edit_message', compact('title', 'Message'));
    }

    public function change_status(Request $request)
    {
    	// \Log::info($request->all());
        $user = Message::find($request->id);
        $user->status = $request->status;
        $user->save();
  
        return response()->json(['success'=>'Status change successfully.']);
    }

    public function store_message(Request $request){

        $UserId = Session::get('ResidentId'); 
        $CommunityId = Session::get('CommunityId'); 

        $Message = new Message();

        $Message->user_id = $UserId;
        $Message->community_id = $CommunityId;
        $Message->description = $request->description;
        $Message->status = 1;
        

        $AddMessage = $Message->save();

        return redirect()->back()->with('message','Messages Added Successfully');
    }

    public function update_message(Request $request){

        $UserId = Session::get('ResidentId');
        $CommunityId = Session::get('CommunityId'); 
        
        $id = $request->id;
        $Message = Message::where('id', $id)->first();
         
        $Message->community_id = $CommunityId;
        $Message->description = $request->description;
        

        $AddMessage = $Message->save();

        return redirect()->back()->with('message','Messages Updated Successfully');
    }

    public function my_profile(){
        
        $title = "Managers Profile";
        $ResidentId = Session::get('ResidentId');  
        $Managers = Users::select('users.*')
                    ->where('id', $ResidentId)
                    ->first();

        // $Groups = Groups::count();
        return view('residents.profile.my_profile', compact('title', 'Managers'));
    }

    public function update_profile(Request $request){

        $ResidentId = Session::get('ResidentId');  
        $Users = Users::where('id', $ResidentId)->first();

        $Users->email = $request->email;
        $Users->mobile = $request->mobile;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/residents/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','Resident Profile Updated Successfully');
    }

    public function update_password(Request $request)
    {
 
        //  $this->validate($request, [
 
        // 'oldpassword' => 'required',
        // 'newpassword' => 'required',
        // ]);
 
        $UserId = Session::get('ResidentId');
 
       $hashedPassword = Auth::guard('resident')->user()->password;
 
    //    if (\Hash::check($request->oldpassword , $hashedPassword )) {
 
         if (!\Hash::check($request->newpassword , $hashedPassword)) {
 
              $users =Users::find($UserId);
              $users->password = bcrypt($request->newpassword);
              Users::where( 'id' , $UserId)->update( array( 'password' =>  $users->password));
 
              session()->flash('message','password updated successfully');
              return redirect()->back();
            }
 
 
       }
}
