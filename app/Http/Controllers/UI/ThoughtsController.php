<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Thoughts;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class ThoughtsController extends Controller
{
    public function list(){
        
        $title = "News List";
        // $UserId = Session::get('TeacherId'); 
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
            $Thoughts = Thoughts::orderBy('created_at', 'DESC')->get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');       
            
            $Thoughts = Thoughts::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }

        // $Groups = Groups::count();
        return view('Admin.thoughts.list', compact('title', 'Thoughts'));
    }

    public function add_thoughts(){
        
        $title = "Add Thoughts";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.thoughts.add_thoughts', compact('title'));
    }

    public function edit_thoughts($id){
        
        $title = "Edit Thoughts";
        // $UserId = Session::get('TeacherId');  

        $Thoughts = Thoughts::where('id', $id)->first();
        return view('Admin.thoughts.edit_thoughts', compact('title', 'Thoughts'));
    }

    public function delete_thoughts($id){
        
        $Thoughts = Thoughts::where('id', $id)->delete();
        return redirect()->back()->with('message','News & Updates Deleted Successfully');
        
    }

    public function store_thoughts(Request $request){
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $Thoughts = new Thoughts();

        $Thoughts->user_id = $UserId;
        $Thoughts->title = $request->title;
        $Thoughts->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/thoughts/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Thoughts->image = $filename;
        }

        $Thoughts->status = 1;

        $AddThoughts = $Thoughts->save();

        return redirect()->back()->with('message','Thoughts Added Successfully');
    }

    public function update_thoughts(Request $request){
        // if(Auth::guard('super_admin')->check()){       
        //     $UserId = Session::get('AdminId');         
        // }elseif(Auth::guard('manager')->check()){
        //     $UserId = Session::get('ManagerId');         
        // }
        $id = $request->id;
        $Thoughts = Thoughts::where('id', $id)->first();

        // $Events->user_id = $UserId;
        $Thoughts->title = $request->title;
        $Thoughts->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/thoughts/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Thoughts->image = $filename;
        }else{
            $Thoughts->image = $Thoughts->image;
        }

        $AddThoughts = $Thoughts->save();

        return redirect()->back()->with('message','Thoughts Updated Successfully');
    }
}
