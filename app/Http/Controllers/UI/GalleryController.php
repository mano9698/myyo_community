<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Gallery;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class GalleryController extends Controller
{
    public function list(){
        
        $title = "Gallery List";
        // $UserId = Session::get('TeacherId');  
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
            $Gallery = Gallery::get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');       
            
            $Gallery = Gallery::where('user_id', $UserId)->get();

        }

        // $Groups = Groups::count();
        return view('Admin.gallery.list', compact('title', 'Gallery'));
    }

    public function add_gallery(){
        
        $title = "Add Gallery";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.gallery.add_gallery', compact('title'));
    }

    public function edit_gallery($id){
        
        $title = "Edit Gallery";
        // $UserId = Session::get('TeacherId');  

        $Gallery = Gallery::where('id', $id)->first();
        return view('Admin.gallery.edit_gallery', compact('title', 'Gallery'));
    }

    public function delete_gallery($id){
        
        $Gallery = Gallery::where('id', $id)->delete();
        return redirect()->back()->with('message','Gallery Deleted Successfully');
        
    }

    public function store_gallery(Request $request){

        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $Gallery = new Gallery();

        $Gallery->user_id = $UserId;
        $Gallery->title = $request->title;
        $Gallery->date = $request->date;
        $Gallery->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/gallery/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Gallery->image = $filename;
        }

        $Gallery->status = 1;

        $AddGallery = $Gallery->save();

        return redirect()->back()->with('message','Gallery Added Successfully');
    }

    public function update_gallery(Request $request){
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $id = $request->id;
        $Gallery = Gallery::where('id', $id)->first();

        $Gallery->user_id = $UserId;
        $Gallery->title = $request->title;
        $Gallery->date = $request->date;
        $Gallery->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/gallery/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Gallery->image = $filename;
        }else{
            $Gallery->image = $Gallery->image;
        }

        $Gallery->status = 1;

        $AddGallery = $Gallery->save();

        return redirect()->back()->with('message','Gallery Updated Successfully');
    }
}
