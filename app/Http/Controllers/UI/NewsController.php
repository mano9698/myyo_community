<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\NewsUpdates;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class NewsController extends Controller
{
    public function list(){
        
        $title = "News List";
        // $UserId = Session::get('TeacherId'); 
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
            $NewsUpdates = NewsUpdates::orderBy('created_at', 'DESC')->get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');       
            
            $NewsUpdates = NewsUpdates::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }

        // $Groups = Groups::count();
        return view('Admin.news.list', compact('title', 'NewsUpdates'));
    }

    public function add_news(){
        
        $title = "Add News";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.news.add_news', compact('title'));
    }

    public function edit_news($id){
        
        $title = "Edit News";
        // $UserId = Session::get('TeacherId');  

        $NewsUpdates = NewsUpdates::where('id', $id)->first();
        return view('Admin.news.edit_news', compact('title', 'NewsUpdates'));
    }

    public function delete_news($id){
        
        $NewsUpdates = NewsUpdates::where('id', $id)->delete();
        return redirect()->back()->with('message','News & Updates Deleted Successfully');
        
    }

    public function store_news(Request $request){
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $NewsUpdates = new NewsUpdates();

        $NewsUpdates->user_id = $UserId;
        $NewsUpdates->title = $request->title;
        $NewsUpdates->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/news/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $NewsUpdates->image = $filename;
        }

        $NewsUpdates->status = 1;

        $AddNewsUpdates = $NewsUpdates->save();

        return redirect()->back()->with('message','News & Updates Added Successfully');
    }

    public function update_news(Request $request){
        // if(Auth::guard('super_admin')->check()){       
        //     $UserId = Session::get('AdminId');         
        // }elseif(Auth::guard('manager')->check()){
        //     $UserId = Session::get('ManagerId');         
        // }
        $id = $request->id;
        $NewsUpdates = NewsUpdates::where('id', $id)->first();

        // $Events->user_id = $UserId;
        $NewsUpdates->title = $request->title;
        $NewsUpdates->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/news/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $NewsUpdates->image = $filename;
        }else{
            $NewsUpdates->image = $NewsUpdates->image;
        }

        $AddNewsUpdates = $NewsUpdates->save();

        return redirect()->back()->with('message','News & Updates Updated Successfully');
    }
}
