<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\LearningActivities;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class LearningActivitiesController extends Controller
{
    public function list(){
        
        $title = "Learning Activities List";
        // $UserId = Session::get('TeacherId');  
        $LearningActivities = LearningActivities::get();

        // $Groups = Groups::count();
        return view('Admin.learn.list', compact('title', 'LearningActivities'));
    }

    public function add_learn(){
        
        $title = "Add Learning Activities List";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.learn.add_learn', compact('title'));
    }

    public function edit_learn($id){
        
        $title = "Edit Learning Activities";
        // $UserId = Session::get('TeacherId');  

        $LearningActivities = LearningActivities::where('id', $id)->first();
        return view('Admin.learn.edit_learn', compact('title', 'LearningActivities'));
    }

    public function delete_learn($id){
        
        $LearningActivities = LearningActivities::where('id', $id)->delete();
        return redirect()->back()->with('message','Learning Activities Deleted Successfully');
        
    }

    public function store_learn(Request $request){
        $LearningActivities = new LearningActivities();

        $LearningActivities->title = $request->title;
        $LearningActivities->date = $request->date;
        $LearningActivities->time = $request->time;
        $LearningActivities->speaker = $request->speaker;
        $LearningActivities->price = $request->price;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/learn/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $LearningActivities->image = $filename;
        }

        $LearningActivities->status = 1;

        $AddLearningActivities = $LearningActivities->save();

        return redirect()->back()->with('message','Learning Activities Added Successfully');
    }

    public function update_learn(Request $request){
        // if(Auth::guard('super_admin')->check()){       
        //     $UserId = Session::get('AdminId');         
        // }elseif(Auth::guard('manager')->check()){
        //     $UserId = Session::get('ManagerId');         
        // }
        $id = $request->id;
        $LearningActivities = LearningActivities::where('id', $id)->first();

        $LearningActivities->title = $request->title;
        $LearningActivities->date = $request->date;
        $LearningActivities->time = $request->time;
        $LearningActivities->speaker = $request->speaker;
        $LearningActivities->price = $request->price;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/learn/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $LearningActivities->image = $filename;
        }else{
            $LearningActivities->image = $LearningActivities->image;
        }

        $LearningActivities->status = 1;

        $AddLearningActivities = $LearningActivities->save();

        return redirect()->back()->with('message','Learning Activities Updated Successfully');
    }
}
