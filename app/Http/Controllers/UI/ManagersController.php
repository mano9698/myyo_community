<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\Community;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class ManagersController extends Controller
{
    public function list(){
        
        $title = "Managers List";
        // $UserId = Session::get('TeacherId');  
        $Managers = Users::select('users.*', 'community.community_name')
                    ->join('community', 'community.id', '=', 'users.community_id')
                    ->where('user_type', 2)
                    ->get();

        // $Groups = Groups::count();
        return view('Admin.managers.list', compact('title', 'Managers'));
    }

    public function add_manager(){
        
        $title = "Add Manager";
        // $UserId = Session::get('TeacherId');  

        $Community = Community::get();
        return view('Admin.managers.add_manager', compact('title', 'Community'));
    }

    public function edit_manager($id){
        
        $title = "Edit Manager";
        // $UserId = Session::get('TeacherId');  

        $Managers = Users::where('id', $id)->first();
        $Community = Community::get();
        return view('Admin.managers.edit_manager', compact('title', 'Managers', 'Community'));
    }

    public function change_status(Request $request)
    {
    	// \Log::info($request->all());
        $user = Users::find($request->id);
        $user->status = $request->status;
        $user->save();
  
        return response()->json(['success'=>'Status change successfully.']);
    }

    // public function delete_manager($id){
        
    //     $Community = Community::where('id', $id)->delete();
    //     return redirect()->back()->with('message','Community Deleted Successfully');
        
    // }

    public function store_manager(Request $request){
        $Users = new Users();

        $Community = Community::where('id', $request->community_id)->first();

        if($Community){
            return redirect()->back()->with('message','This community is already updated in another manager...');
            
        }else{
            $Users->community_id = $request->community_id;
            $Users->first_name = $request->first_name;
            $Users->last_name = $request->last_name;
            $Users->email = $request->email;
            $Users->password = Hash::make("Test@123");
            $Users->user_type = 2;
    
            if($request->hasfile('image')){
                $extension = $request->file('image')->getClientOriginalExtension();
                $dir = 'Admin/managers/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('image')->move($dir, $filename);
        
                $Users->profile_pic = $filename;
            }
    
            $Users->status = 1;
    
            $AddUsers = $Users->save();
    
            return redirect()->back()->with('message','Manager Added Successfully');

        }

       
    }

    public function update_manager(Request $request){
        // if(Auth::guard('super_admin')->check()){       
        //     $UserId = Session::get('AdminId');         
        // }elseif(Auth::guard('manager')->check()){
        //     $UserId = Session::get('ManagerId');         
        // }
        

        $Community = Community::where('id', $request->community_id)->first();

        if($Community){
            return redirect()->back()->with('message','This community is already updated in another manager...');
        }else{
            $id = $request->id;
            $Users = Users::where('id', $id)->first();

            $Users->community_id = $request->community_id;
            $Users->first_name = $request->first_name;
            $Users->last_name = $request->last_name;
            $Users->email = $request->email;

            if($request->hasfile('image')){
                $extension = $request->file('image')->getClientOriginalExtension();
                $dir = 'Admin/managers/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('image')->move($dir, $filename);
        
                $Users->profile_pic = $filename;
            }else{
                $Users->profile_pic = $Users->profile_pic;
            }

            $Users->status = 1;

            $AddUsers = $Users->save();

            return redirect()->back()->with('message','Manager Updated Successfully');

        }

        
    }
}
