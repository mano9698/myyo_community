<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Community;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;
use Illuminate\Support\Str;

class CommunityController extends Controller
{
    public function list(){
        
        $title = "Community List";
        // $UserId = Session::get('TeacherId');  
        $Community = Community::get();

        // $Groups = Groups::count();
        return view('Admin.community.list', compact('title', 'Community'));
    }

    public function add_community(){
        
        $title = "Add Community";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.community.add_community', compact('title'));
    }

    public function edit_community($id){
        
        $title = "Edit Community";
        // $UserId = Session::get('TeacherId');  

        $Community = Community::where('id', $id)->first();
        return view('Admin.community.edit_community', compact('title', 'Community'));
    }

    public function delete_community($id){
        
        $Community = Community::where('id', $id)->delete();
        return redirect()->back()->with('message','Community Deleted Successfully');
        
    }

    public function store_community(Request $request){
        $Community = new Community();

        $Community->community_name = $request->community_name;
        $Community->slug = Str::slug($request->community_name);
        $Community->no_of_flats = $request->no_of_flats;
        $Community->country = $request->country;
        $Community->state = $request->state;
        $Community->city = $request->city;
        $Community->postal_code = $request->postal_code;
        $Community->area = $request->area;
        $Community->address = $request->address;
        $Community->status = 1;

        $AddCommunity = $Community->save();

        return redirect()->back()->with('message','Community Added Successfully');
    }

    public function update_community(Request $request){
        // if(Auth::guard('super_admin')->check()){       
        //     $UserId = Session::get('AdminId');         
        // }elseif(Auth::guard('manager')->check()){
        //     $UserId = Session::get('ManagerId');         
        // }
        $id = $request->id;
        $Community = Community::where('id', $id)->first();

        $Community->community_name = $request->community_name;
        $Community->slug = Str::slug($request->community_name);
        $Community->no_of_flats = $request->no_of_flats;
        $Community->country = $request->country;
        $Community->state = $request->state;
        $Community->city = $request->city;
        $Community->postal_code = $request->postal_code;
        $Community->area = $request->area;
        $Community->address = $request->address;
        $Community->status = 1;

        $AddCommunity = $Community->save();

        return redirect()->back()->with('message','Community Updated Successfully');
    }
}
