<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\Community;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class ResidentsController extends Controller
{
    public function list(){
        
        $title = "Residents List";
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
            $Residents = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');       
            
            $Residents = Users::select('users.*', 'community.community_name')
                    ->join('community', 'community.id', '=', 'users.community_id')
                    ->where('users.id', $UserId)
                    ->where('users.user_type', 3)
                    ->get();
        }

        // $Groups = Groups::count();
        return view('Admin.residents.list', compact('title', 'Residents'));
    }

    public function add_residents(){
        
        $title = "Add Residents";
        // $UserId = Session::get('TeacherId');  

        $Community = Community::get();
        return view('Admin.residents.add_residents', compact('title', 'Community'));
    }

    public function edit_residents($id){
        
        $title = "Edit Residents";
        // $UserId = Session::get('TeacherId');  

        $Residents = Users::where('id', $id)->first();
        $Community = Community::get();
        return view('Admin.residents.edit_residents', compact('title', 'Residents', 'Community'));
    }

    public function change_status(Request $request)
    {
    	// \Log::info($request->all());
        $user = Users::find($request->id);
        $user->status = $request->status;
        $user->save();
  
        return response()->json(['success'=>'Status change successfully.']);
    }

    // public function delete_manager($id){
        
    //     $Community = Community::where('id', $id)->delete();
    //     return redirect()->back()->with('message','Community Deleted Successfully');
        
    // }

    public function store_residents(Request $request){
        $Community = Community::where('id', $request->community_id)->first();

        if($Community){
            return redirect()->back()->with('message','This community is already updated in another residents...');
            
        }else{
            $Users = new Users();

            $Users->community_id = $request->community_id;
            $Users->first_name = $request->first_name;
            $Users->last_name = $request->last_name;
            $Users->email = $request->email;
            $Users->password = Hash::make("Test@123");
            $Users->flat_no = $request->flat_no;
            $Users->user_type = 3;
    
            if($request->hasfile('image')){
                $extension = $request->file('image')->getClientOriginalExtension();
                $dir = 'Admin/residents/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('image')->move($dir, $filename);
        
                $Users->profile_pic = $filename;
            }
    
            $Users->status = 1;
    
            $AddUsers = $Users->save();
    
            return redirect()->back()->with('message','Residents Added Successfully');
        }
        
    }

    public function update_residents(Request $request){
        // $Community = Community::where('id', $request->community_id)->first();
        // if(Auth::guard('super_admin')->check()){       
        //     $UserId = Session::get('AdminId');         
        // }elseif(Auth::guard('manager')->check()){
        //     $UserId = Session::get('ManagerId');         
        // }
        // if($Community){
        //     return redirect()->back()->with('message','This community is already updated in another residents...');
            
        // }else{
            $id = $request->id;
            $Users = Users::where('id', $id)->first();
    
            // $Users->community_id = $request->community_id;
            $Users->first_name = $request->first_name;
            $Users->last_name = $request->last_name;
            $Users->email = $request->email;
            $Users->flat_no = $request->flat_no;
    
            if($request->hasfile('image')){
                $extension = $request->file('image')->getClientOriginalExtension();
                $dir = 'Admin/residents/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('image')->move($dir, $filename);
        
                $Users->profile_pic = $filename;
            }else{
                $Users->profile_pic  = $Users->profile_pic ;
            }
    
            $AddUsers = $Users->save();
    
            return redirect()->back()->with('message','Residents Updated Successfully');
        // }
        
    }
}
