<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Events;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class EventsController extends Controller
{
    public function list(){
        
        $title = "Events List";
        // $UserId = Session::get('TeacherId'); 
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
            $Events = Events::orderBy('created_at', 'DESC')->get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');       
            
            $Events = Events::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }

        // $Groups = Groups::count();
        return view('Admin.events.list', compact('title', 'Events'));
    }

    public function add_events(){
        
        $title = "Add Events";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.events.add_events', compact('title'));
    }

    public function edit_events($id){
        
        $title = "Edit Events";
        // $UserId = Session::get('TeacherId');  

        $Events = Events::where('id', $id)->first();
        return view('Admin.events.edit_events', compact('title', 'Events'));
    }

    public function delete_events($id){
        
        $Events = Events::where('id', $id)->delete();
        return redirect()->back()->with('message','Events Deleted Successfully');
        
    }

    public function store_events(Request $request){
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $Events = new Events();

        $Events->user_id = $UserId;
        $Events->title = $request->title;
        $Events->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/events/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Events->image = $filename;
        }

        $Events->status = 1;

        $AddEvents = $Events->save();

        return redirect()->back()->with('message','Events Added Successfully');
    }

    public function update_events(Request $request){
        // if(Auth::guard('super_admin')->check()){       
        //     $UserId = Session::get('AdminId');         
        // }elseif(Auth::guard('manager')->check()){
        //     $UserId = Session::get('ManagerId');         
        // }
        $id = $request->id;
        $Events = Events::where('id', $id)->first();

        // $Events->user_id = $UserId;
        $Events->title = $request->title;
        $Events->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/events/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Events->image = $filename;
        }else{
            $Events->image = $Events->image;
        }

        $AddEvents = $Events->save();

        return redirect()->back()->with('message','Events Updated Successfully');
    }
}
