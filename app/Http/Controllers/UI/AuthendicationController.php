<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\Community;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class AuthendicationController extends Controller
{
    public function login(){
        $title = "Login";

        return view('Admin.login');
    }

    public function admin_login(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckEmail = Users::where('email', $Email)->first();

        // $request->session()->put('last_login_timestamp', time());

        if($CheckEmail == null){

            return redirect()->back()->with('message','Please check your credentials');

        }else{

            if($CheckEmail->status == 0){

                return redirect()->back()->with('message','Your account is not activated. Please contact your administrator...');
                    
            }elseif($CheckEmail->user_type == 1){
                
                if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('AdminName', Auth::guard('super_admin')->user()->first_name);
                    $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                    $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');
                    
                }
            }elseif($CheckEmail->user_type == 2){
                if (Auth::guard('manager')->attempt(['email' => $Email, 'password' => $Password])) {

                    $Community = Community::where('id', Auth::guard('manager')->user()->community_id)->first();

                    $request->session()->put('Managername', Auth::guard('manager')->user()->first_name);
                    $request->session()->put('ManagerEmail', Auth::guard('manager')->user()->email);
                    $request->session()->put('ManagerId', Auth::guard('manager')->user()->id);

                    $request->session()->put('CommunityName', $Community->slug);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('managers/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');
                    
                }
            }
            elseif($CheckEmail->user_type == 3){
                if (Auth::guard('resident')->attempt(['email' => $Email, 'password' => $Password])) {
                    $Community = Community::where('id', Auth::guard('resident')->user()->community_id)->first();

                    $request->session()->put('Residentname', Auth::guard('resident')->user()->first_name);
                    $request->session()->put('ResidentEmail', Auth::guard('resident')->user()->email);
                    $request->session()->put('ResidentId', Auth::guard('resident')->user()->id);

                    $request->session()->put('CommunityName', $Community->slug);
                    $request->session()->put('CommunityId', $Community->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('residents/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');
                    
                }
            }
        }
        
    }
}
