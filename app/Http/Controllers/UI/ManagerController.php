<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\Community;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class ManagerController extends Controller
{
    public function my_profile(){
        
        $title = "Managers Profile";
        $ManagerId = Session::get('ManagerId');  
        $Managers = Users::select('users.*')
                    ->where('id', $ManagerId)
                    ->first();

        // $Groups = Groups::count();
        return view('manager.layouts.my_profile', compact('title', 'Managers'));
    }

    public function update_profile(Request $request){

        $ManagerId = Session::get('ManagerId');  
        $Users = Users::where('id', $ManagerId)->first();

        $Users->email = $request->email;
        $Users->mobile = $request->mobile;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->pincode = $request->pincode;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/managers/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Users->profile_pic = $filename;
        }else{
            $Users->profile_pic = $Users->profile_pic;
        }

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','Manager Profile Updated Successfully');
    }

    public function update_password(Request $request)
    {
 
        //  $this->validate($request, [
 
        // 'oldpassword' => 'required',
        // 'newpassword' => 'required',
        // ]);
 
        $UserId = Session::get('ManagerId');
 
       $hashedPassword = Auth::guard('manager')->user()->password;
 
    //    if (\Hash::check($request->oldpassword , $hashedPassword )) {
 
         if (!\Hash::check($request->newpassword , $hashedPassword)) {
 
              $users =Users::find($UserId);
              $users->password = bcrypt($request->newpassword);
              Users::where( 'id' , $UserId)->update( array( 'password' =>  $users->password));
 
              session()->flash('message','password updated successfully');
              return redirect()->back();
            }
 
        //     else{
        //           session()->flash('message','new password can not be the old password!');
        //           return redirect()->back();
        //         }
 
        //    }
 
 
       }
}
