<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Ads;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\UI\Community;
use App\Models\UI\Users;
use App\Models\UI\Events;

use Session;

class AdminController extends Controller
{
    public function dashboard(){

        $title = "Admin Dashboard";
        // $UserId = Session::get('TeacherId');
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');

            $Residents = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->count();

            $Community = Community::count();

            $Managers = Users::select('users.*', 'community.community_name')
                    ->join('community', 'community.id', '=', 'users.community_id')
                    ->where('user_type', 2)
                    ->count();
            $Events = Events::orderBy('created_at', 'DESC')->count();

        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');

            $Residents = Users::select('users.*', 'community.community_name')
                    ->join('community', 'community.id', '=', 'users.community_id')
                    ->where('users.id', $UserId)
                    ->where('users.user_type', 3)
                    ->count();
            $Community = Community::count();

            $Managers = Users::select('users.*', 'community.community_name')
                    ->join('community', 'community.id', '=', 'users.community_id')
                    ->where('users.id', $UserId)
                    ->where('user_type', 2)
                    ->count();

            $Events = Events::where('user_id', $UserId)->orderBy('created_at', 'DESC')->count();
        }

        // $Groups = Groups::count();
        return view('Admin.layouts.dashboard', compact('title', 'Residents', 'Community', 'Managers', 'Events'));
    }

    public function ads_list(){

        $title = "Ads List";
        // $UserId = Session::get('TeacherId');
        $Ads = Ads::get();

        // $Groups = Groups::count();
        return view('Admin.ads.list', compact('title', 'Ads'));
    }

    public function add_ads(){

        $title = "Add Useful";
        // $UserId = Session::get('TeacherId');

        // $Groups = Groups::count();
        return view('Admin.ads.add_ads', compact('title'));
    }

    public function edit_ads($id){

        $title = "Edit Useful";
        // $UserId = Session::get('TeacherId');

        $Ads = Ads::where('id', $id)->first();
        return view('Admin.ads.edit_ads', compact('title', 'Ads'));
    }

    public function delete_ads($id){

        $Ads = Ads::where('id', $id)->delete();
        return redirect()->back()->with('message','Ads Deleted Successfully');

    }

    public function store_ads(Request $request){

        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
        }

        $Ads = new Ads();

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/ads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);

            $Ads->image = $filename;
        }


        $AddAds = $Ads->save();

        return redirect()->back()->with('message','Ads Added Successfully');
    }

    public function update_ads(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
        }

        $id = $request->id;
        $Ads = Ads::where('id', $id)->first();

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/ads/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);

            $Ads->image = $filename;
        }else{
            $Ads->image = $Ads->image;
        }


        $AddAds = $Ads->save();

        return redirect()->back()->with('message','Ads Updated Successfully');
    }


    public function manager_logout()
    {
        Auth::guard('manager')->logout();
        return redirect('/');
    }


    public function resident_logout()
    {
        Auth::guard('resident')->logout();
        return redirect('/');
    }

    public function admin_logout()
    {
        Auth::guard('super_admin')->logout();
        return redirect('/');
    }
}
