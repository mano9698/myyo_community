<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\Community;
use App\Models\UI\Events;
use App\Models\UI\LearningActivities;
use App\Models\UI\Gallery;
use App\Models\UI\Ads;
use App\Models\UI\Social;
use App\Models\UI\UsefulInfo;
use App\Models\UI\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class HomeController extends Controller
{
    public function community_website($slug){
        
        $title = "Community Website";

        $Community = Community::where('slug', $slug)->first();
        $Users = Users::where('community_id', $Community->id)->first();
        $Events = Events::where('user_id', $Users->id)->orderBy('created_at', 'DESC')->first();

        $LearningActivities = LearningActivities::orderBy('created_at', 'DESC')->get();
        $Ads = Ads::orderBy('created_at', 'DESC')->get();

        $Gallery = Gallery::where('user_id', $Users->id)->orderBy('created_at', 'DESC')->get();
        $Social = Social::where('user_id', $Users->id)->orderBy('created_at', 'DESC')->get();
        $UsefulInfo = UsefulInfo::where('user_id', $Users->id)->orderBy('created_at', 'DESC')->get();
        $Message = Message::where('community_id', $Community->id)->orderBy('created_at', 'DESC')->get();

        // echo json_encode($Message);
        // exit;
        // $Groups = Groups::count();
        return view('ui.home', compact('title', 'Events', 'LearningActivities', 'Gallery', 'Ads', 'Social', 'UsefulInfo', 'Message'));
    }
}
