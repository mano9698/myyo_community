<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Social;
use App\Models\UI\UsefulInfo;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class SocialController extends Controller
{
    public function list(){
        
        $title = "Social List";
        // $UserId = Session::get('TeacherId');  
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
            $Social = Social::get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');       
            
            $Social = Social::where('user_id', $UserId)->get();
        }
        
        // $Groups = Groups::count();
        return view('Admin.social.list', compact('title', 'Social'));
    }

    public function add_social(){
        
        $title = "Add Social";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.social.add_social', compact('title'));
    }

    public function edit_social($id){
        
        $title = "Edit Social";
        // $UserId = Session::get('TeacherId');  

        $Social = Social::where('id', $id)->first();
        return view('Admin.social.edit_social', compact('title', 'Social'));
    }

    public function delete_social($id){
        
        $Social = Social::where('id', $id)->delete();
        return redirect()->back()->with('message','Social Deleted Successfully');
        
    }

    public function store_social(Request $request){

        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $Social = new Social();

        $Social->user_id = $UserId;
        $Social->title = $request->title;
        $Social->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/social/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Social->image = $filename;
        }

        $Social->status = 1;

        $AddSocial = $Social->save();

        return redirect()->back()->with('message','Social Initiatives Added Successfully');
    }

    public function update_social(Request $request){
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $id = $request->id;
        $Social = Social::where('id', $id)->first();

        $Social->user_id = $UserId;
        $Social->title = $request->title;
        $Social->description = $request->description;

        if($request->hasfile('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $dir = 'Admin/social/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('image')->move($dir, $filename);
    
            $Social->image = $filename;
        }

        $Social->status = 1;

        $AddSocial = $Social->save();

        return redirect()->back()->with('message','Social Initiatives Updated Successfully');
    }



    // Usefull Info
    public function useful_list(){
        
        $title = "Useful List";
        // $UserId = Session::get('TeacherId');  
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
            $UsefulInfo = UsefulInfo::get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');       
            $UsefulInfo = UsefulInfo::where('user_id', $UserId)->get();
        }
        
        // $Groups = Groups::count();
        return view('Admin.useful.list', compact('title', 'UsefulInfo'));
    }

    public function add_useful(){
        
        $title = "Add Useful";
        // $UserId = Session::get('TeacherId');  

        // $Groups = Groups::count();
        return view('Admin.useful.add_useful', compact('title'));
    }

    public function edit_useful($id){
        
        $title = "Edit Useful";
        // $UserId = Session::get('TeacherId');  

        $UsefulInfo = UsefulInfo::where('id', $id)->first();
        return view('Admin.useful.edit_useful', compact('title', 'UsefulInfo'));
    }

    public function delete_useful($id){
        
        $UsefulInfo = UsefulInfo::where('id', $id)->delete();
        return redirect()->back()->with('message','Useful Info Deleted Successfully');
        
    }

    public function store_useful(Request $request){

        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $UsefulInfo = new UsefulInfo();

        $UsefulInfo->user_id = $UserId;
        $UsefulInfo->description = $request->description;
        

        $AddUsefulInfo = $UsefulInfo->save();

        return redirect()->back()->with('message','Useful Info Initiatives Added Successfully');
    }

    public function update_useful(Request $request){
        if(Auth::guard('super_admin')->check()){       
            $UserId = Session::get('AdminId');         
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');         
        }

        $id = $request->id;
        $UsefulInfo = UsefulInfo::where('id', $id)->first();

        $UsefulInfo->user_id = $UserId;
        $UsefulInfo->description = $request->description;

        $AddUsefulInfo = $UsefulInfo->save();

        return redirect()->back()->with('message','Useful Info Initiatives Updated Successfully');
    }
}
